<%@ page import="com.ibm.security.appscan.bigbucks.model.Portfolio" %>
<%@ page import="com.ibm.security.appscan.bigbucks.util.DBUtil" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="com.ibm.security.appscan.bigbucks.model.Stock" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="com.ibm.security.appscan.bigbucks.model.TransactionSummary" %>
<%@ page import="com.ibm.security.appscan.bigbucks.util.APIUtil" %><%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 4/7/22
  Time: 3:43 AM
  To change this template use File | Settings | File Templates.
--%>
<jsp:include page="/header.jspf"/>

<div id="wrapper" style="width: 99%;">
    <jsp:include page="/bank/membertoc.jspf"/>
    <td valign="top" colspan="3" class="bb">
        <h1>Overall Positions</h1>

        <table width="590" border="0">
            <tr>
                <td>
                    <br><b>All Users' Portfolio</b>
                    <table border=1 cellpadding=2 cellspacing=0 width='590'>
                        <tr>
                            <th>Symbol </th>
                            <th>Name</th>
                            <th>Shares held</th>
                            <th>Price per share</th>
                        </tr>
                        <%
                            ArrayList<Portfolio> portfolios = DBUtil.getAllUsersPortfolios();
                            for (Portfolio portfolio: portfolios){
                                double dblprice = portfolio.getBuyinPrice();
                                String dollarFormat = (dblprice<1)?"$0.00":"$.00";
                                String price = new DecimalFormat(dollarFormat).format(dblprice);
                        %>
                        <tr>
                            <td><%=portfolio.getStockId()%></td>
                            <td><%=Stock.getStock(portfolio.getStockId()).getStockName()%></td>
                            <td><%=portfolio.getShare()%></td>
                            <td><%=price%></td>
                        </tr>
                        <% } %>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br><b>Transaction Summary for <%=APIUtil._getToday()%></b>
                    <table border=1 cellpadding=2 cellspacing=0 width='590'>
                        <tr>
                            <th width="100">Symbol</th>
                            <th width="290">Name </th>
                            <th width="100">Shares bought</th>
                            <th width="100">Shares sold</th>
                        </tr>
                        <%
                            ArrayList<TransactionSummary> tss = DBUtil.get1dayAllTransactions(APIUtil._getToday());
                            for (TransactionSummary ts: tss){
                        %>
                        <tr>
                            <td><%=ts.getStockId()%></td>
                            <td><%=Stock.getStock(ts.getStockId()).getStockName()%></td>
                            <td><%=ts.getBuyShare()%></td>
                            <td><%=ts.getSellShare()%></td>
                        </tr>
                        <% } %>
                    </table>
                </td>
            </tr>
        </table>

</div>
</td>
</div>

<jsp:include page="/footer.jspf"/>
