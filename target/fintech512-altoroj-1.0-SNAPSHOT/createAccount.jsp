<%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 3/7/22
  Time: 2:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<jsp:include page="header.jspf"/>

<div id="wrapper" style="width: 99%;">
    <jsp:include page="/toc.jspf"/>
    <td valign="top" colspan="3" class="bb">
        <div class="fl" style="width: 99%;">

            <h1>Create Account for Online Stock Trading</h1>

            <!-- Be careful what you change.  All changes are made directly to AltoroJ database. -->

            <p><span style="color:#FF0066;font-size:12pt;font-weight:bold;">
            <%
                java.lang.String error = (String)request.getSession().getAttribute("message");

                if (error != null && error.trim().length() > 0){
                    request.getSession().removeAttribute("message");
                    out.print(error);
                }
            %>
            </span></p>

            <form action="createAccount" method="post" name="createAccount" id="createAccount" onsubmit="return (confirminput(createAccount));">
                <table>
                    <tr>
                        <td>
                            First Name:
                        </td>
                        <td>
                            <input type="text" name="firstname" style="width: 150px;">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Last Name:
                        </td>
                        <td>
                            <input type="text" name="lastname" style="width: 150px;">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            User Name:
                        </td>
                        <td>
                            <input type="text" name="username" style="width: 150px;">
                        </td>
                        <td>
                        </td>
                        <td colspan="4">It is highly recommended that you leave the username as first
                            initial last name. Example: "John Smith" could be "jsmith".
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <input type="password" name="password1" value="" style="width: 150px;">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Confirm Password:
                        </td>
                        <td>
                            <input type="password" name="password2" value="" style="width: 150px;">
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="create" value="Create Account">
                        </td>
                    </tr>
                </table>
            </form>

        </div>
        <script type="text/javascript">


            function confirminput(myform) {
                if (myform.firstname.value.length && myform.lastname.value.length && myform.username.value.length && myform.password1.value.length && myform.password2.value.length && myform.password1.value === myform.password2.value) {
                    return (true);
                } else if (!(myform.firstname.value.length)) {
                    myform.reset();
                    myform.firstname.focus();
                    alert ("You must enter a valid first name");
                    return (false);
                } else if (!(myform.lastname.value.length)){
                    myform.lastname.focus();
                    alert ("You must enter a valid last name");
                    return (false);
                } else if (!(myform.username.value.length)){
                    myform.username.focus();
                    alert ("You must enter a valid username");
                    return (false);
                } else if (!(myform.password1.value.length)){
                    myform.password1.focus();
                    alert ("You must enter a valid password");
                    return (false);
                } else {
                    myform.password2.focus();
                    alert ("The two passwords you typed do not match.")
                    return (false);
                }
            }
            window.onload = setfocus;
        </script>
    </td>
</div>

<jsp:include page="footer.jspf"/>