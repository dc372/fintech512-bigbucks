<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
		 pageEncoding="ISO-8859-1"%>

<%
	java.lang.String error1 = (String)request.getSession().getAttribute("message");

	if (error1 != null && error1.trim().length() > 0){
		request.getSession().removeAttribute("message");
		out.print(error1);
	}

	java.lang.String stockName = request.getParameter("stockId");
%>



<link href="css/login.css" rel="stylesheet" >

<title>Log In | BigBucks</title>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
	<link rel="shortcut icon" href="images/html-logo-blue.png">

	<style data-emotion="css-global"></style><style data-emotion="css"></style><script type="text/javascript" src="https://client-api.arkoselabs.com/v2/7F867EDC-C71B-467F-B0A1-8DCBA5D4D2E3/api.js" data-callback="rhArkoseSetupEnforcement" async=""></script><title>Log In | BigBucks</title><style type="text/css">.Z6wXJz-G-6vcu3I0ZoM_B{box-sizing:border-box;border:0;margin:0;padding:0;overflow:hidden;display:none;z-index:2147483647;pointer-events:none;visibility:hidden;opacity:0;transition:opacity 300ms linear;height:0;width:0}.Z6wXJz-G-6vcu3I0ZoM_B.active{display:block;visibility:visible}.Z6wXJz-G-6vcu3I0ZoM_B.active.show{opacity:1;pointer-events:inherit;position:inherit}.Z6wXJz-G-6vcu3I0ZoM_B.active.show.in-situ{width:inherit;height:inherit}.Z6wXJz-G-6vcu3I0ZoM_B.active.show.lightbox{position:fixed;width:100% !important;height:100% !important;top:0;right:0;bottom:0;left:0}@-moz-document url-prefix(''){.Z6wXJz-G-6vcu3I0ZoM_B{visibility:visible;display:block}}
</style></head>

<body class="theme-open-up">
<div id="react_root">
	<div>
		<div>

		</div>
		<div class="css-10d7enp">
			<div class="css-eyq2zi">
				<div class="css-17exwhe">
					<img aria-hidden="true" data-test-id="default-image" sizes="(min-width: 768px) 1440px, 720px" src="img/login.jpeg" class="css-1ox8jnp">
					<div class="css-17gd2ko">
						<div class="css-1lmgvjv">
							<div>
								<form action="doLogin" method="post" name="login" id="login" onsubmit="return (confirminput(login));">
									<input type="hidden" name="stockId" value=<%=stockName%>>
									<header class="css-52qs92">
										<span class="css-1vd3m2i">Log in to BigBucks</span>
									</header>
									<div class="css-1xipndn">
										<div>
											<div class="css-i3pbo" style="margin-bottom: 24px;">
												<div class="css-1fky4ua" >
													<label class="css-8atqhb">
														<div class="css-1te2hl9">
															<span class="css-o2h1av">Username</span>
														</div>
														<div class="css-cgvuc8-InternalInput">
															<input aria-describedby="" autocapitalize="off" autocomplete="username" autocorrect="off" name="uid" required="" spellcheck="false" class="remove-legacy css-1gulmj0-InternalInput" type="text" value="" id="uid">
														</div>
													</label>
												</div>
											</div>
											<div class="css-1upilqn">
												<div class="css-13xzgk4">
													<label class="css-8atqhb">
														<div class="css-1te2hl9">
															<span class="css-o2h1av">Password</span>
														</div>
														<div class="css-13wonh0-InternalInput">
															<input aria-describedby="" autocomplete="current-password"  required="" aria-invalid="false" class="remove-legacy css-1tvo6io-InternalInput" type="password" value="" id="passw" name="passw">
															<button type="button" aria-label="show password in plain text" aria-pressed="false" aria-busy="false" class="css-1y4djg4-UnstyledButton-BaseButton">
																<span class="css-1lg9j9q-InternalButtonContent">
																	<span class="css-1w49doo-InternalButtonContent">
																		<span aria-hidden="true" class="css-u1uuto-iconElement">
																			<svg fill="none" height="16" role="img" viewBox="0 0 16 16" width="16" xmlns="http://www.w3.org/2000/svg">
																				<path clip-rule="evenodd" d="M1 7.99996C1 7.99996 2.90909 3.54541 8 3.54541C13.0909 3.54541 15 7.99996 15 7.99996C15 7.99996 13.0909 12.4545 8 12.4545C2.90909 12.4545 1 7.99996 1 7.99996ZM5.77254 8.00002C5.77254 9.2282 6.77163 10.2273 7.99982 10.2273C9.228 10.2273 10.2271 9.2282 10.2271 8.00002C10.2271 6.77184 9.228 5.77275 7.99982 5.77275C6.77163 5.77275 5.77254 6.77184 5.77254 8.00002Z" fill="var(--rh__text-color)" fill-rule="evenodd">

																				</path>
																			</svg>
																		</span>
																	</span>
																</span>
															</button>
														</div>
													</label>
												</div>
											</div>
										</div>
										<p>
											<a role="link" class="rh-hyperlink css-mwxq27-UnstyledAnchor-inlineButtonStyles-BaseButton" rel="" href="#">
												<span class="css-1lg9j9q-InternalButtonContent">
													<span class="css-1w49doo-InternalButtonContent">
														<span class="css-i6yoyz">Forgot your password?</span>
													</span>
												</span>
											</a>
										</p>

										<%
											java.lang.String error = (String)request.getSession(true).getAttribute("loginError");

											if (error != null && error.trim().length() > 0){
												request.getSession().removeAttribute("loginError");
												out.print("<p aria-live=\"assertive\" data-testid=\"LoginErrorMessage\">" +
																"<div class=\"_2QawT-EkOq7gIQN1hHcFqU\" style=\"height: 20px; transition-duration: 300ms;\">\n" +
																"<div class=\"_3UC6vIw0Z8kgiibABGCPT2\">" +
																"<div>" +
																"<div class=\"_69VHCtVZwuwDirIVL0vhD zPyZmsoEi30X3b7ulwJa9\" style=\"transition-duration: 250ms;\">\n" +
																"<h4 class=\"_311oZ7RLN6n5KWY6Q21_rc\">" +
																"<div class=\"_336VF-3Q48jM9ZaUFqhXEz\">" +
																"<svg fill=\"none\" height=\"16\" role=\"img\" viewBox=\"0 0 16 16\" width=\"16\" xmlns=\"http://www.w3.org/2000/svg\">" +
																"<path clip-rule=\"evenodd\" d=\"M8 2.5C4.97843 2.5 2.5 4.97843 2.5 8C2.5 11.0216 4.97843 13.5 8 13.5C11.0216 13.5 13.5 11.0216 13.5 8C13.5 4.97843 11.0216 2.5 8 2.5ZM1 8C1 4.15 4.15 0.999999 8 1C11.85 1 15 4.15 15 8C15 11.85 11.85 15 8 15C4.15 15 0.999999 11.85 1 8Z\" fill=\"var(--rh__text-color)\" fill-rule=\"evenodd\">" +
																"</path>" +
																"<path clip-rule=\"evenodd\" d=\"M7.25 10V11.5H8.75V10H7.25ZM7.25 8.5L8.75 8.5L8.75 4.5H7.25V8.5Z\" fill=\"var(--rh__text-color)\" fill-rule=\"evenodd\"></path>" +
																"</svg>" +
																"</div>" +
																"<div>" +
																"<span class=\"css-w8p71j\">"+

														error + "</span>\n" +
														"\t\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
														"\t\t\t\t\t\t\t\t\t\t\t\t\t</h4>\n" +
														"\t\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
														"\t\t\t\t\t\t\t\t\t\t\t</div>\n" +
														"\t\t\t\t\t\t\t\t\t\t</div>\n" +
														"\t\t\t\t\t\t\t\t\t</div>\n" +
														"\t\t\t\t\t\t\t\t\t\t</p>");
											}
										%>


									</div>
									<footer class="css-1c7gf0b">
										<div id="submitbutton" class="css-0">
											<div class="css-1jzivk7">
												<button type="submit" aria-busy="false" class="css-jowa3o-UnstyledButton-BaseButton">
													<span class="css-1lg9j9q-InternalButtonContent">
														<span class="css-1w49doo-InternalButtonContent">
															<span class="css-w8p71j">Log In</span>
														</span>
													</span>
												</button>
											</div>
										</div>
										<div style="margin-top: 25px;">
											<span class="css-o2h1av">Not on BigBucks?</span>
											<a role="link" class="rh-hyperlink css-1nedewm-UnstyledAnchor-inlineButtonStyles-BaseButton" rel="" href="createAccount">
												<span class="css-1lg9j9q-InternalButtonContent">
													<span class="css-1w49doo-InternalButtonContent">
														<span class="css-eaou0n">Create an account</span>
													</span>
												</span>
											</a>
										</div>
										<br/>
										<div style="margin-top: 25px; text-align: right;" >
											<a role="link" class="rh-hyperlink css-1nedewm-UnstyledAnchor-inlineButtonStyles-BaseButton" rel="" href="index.jsp?default.htm">
												<span class="css-1lg9j9q-InternalButtonContent">
													<span class="css-1w49doo-InternalButtonContent">
														<span class="css-eaou0na"><b>Back to Main ></b></span>
													</span>
												</span>
											</a>
										</div>
									</footer>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div></div></div><div></div><div></div></div>
<div id="ethnio"></div>

<div aria-hidden="true">
	<iframe src="https://client-api.arkoselabs.com/v2/7F867EDC-C71B-467F-B0A1-8DCBA5D4D2E3/enforcement.9d8a79f2ef8a7d6aacf9b0b9b8f0a835.html" class="Z6wXJz-G-6vcu3I0ZoM_B lightbox" title="Verification challenge" data-e2e="enforcement-frame">
	</iframe>
</div>
</body>

<%--<script src="js/Vendor-317794a741e3c1c335a5.js" nonce=""></script>--%>

<script type="text/javascript">
	function setfocus() {
		if (document.login.uid.value=="") {
			document.login.uid.focus();
		} else {
			document.login.passw.focus();
		}
	}

	function confirminput(myform) {
		if (myform.uid.value.length && myform.passw.value.length) {
			return (true);
		} else if (!(myform.uid.value.length)) {
			myform.reset();
			myform.uid.focus();
			alert ("You must enter a valid username");
			return (false);
		} else {
			myform.passw.focus();
			alert ("You must enter a valid password");
			return (false);
		}
	}
	window.onload = setfocus;
</script>
