<%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 4/6/22
  Time: 8:04 PM
  To change this template use File | Settings | File Templates.
--%>
<jsp:include page="/header.jspf"/>

<div id="wrapper" style="width: 99%;">
  <jsp:include page="membertoc.jspf"/>
  <td valign="top" colspan="3" class="bb">
    <%@page import="com.ibm.security.appscan.bigbucks.model.Account"%>
    <%@page import="java.text.SimpleDateFormat"%>
    <%@page import="java.text.NumberFormat"%>
    <%@page import="java.text.DecimalFormat"%>
    <%@page import="java.util.ArrayList"%>
    <%@ page import="com.ibm.security.appscan.bigbucks.model.Portfolio" %>
    <%@ page import="com.ibm.security.appscan.bigbucks.model.Stock" %>
    <%@ page import="com.ibm.security.appscan.bigbucks.model.Profile" %>
    <%@ page import="com.ibm.security.appscan.bigbucks.util.DBUtil" %>
    <div class="fl" style="width: 99%;">

      <%
        com.ibm.security.appscan.bigbucks.model.User user = (com.ibm.security.appscan.bigbucks.model.User)request.getSession().getAttribute("user");
        ArrayList<Account> accounts = new ArrayList<Account>();
        java.lang.String paramName = request.getParameter("acctId");
        String accountName = paramName;

        for (Account account: user.getAccounts()){

          if (!String.valueOf(account.getAccountId()).equals(paramName))
            accounts.add(account);
          else {
            accounts.add(0, account);
            accountName = account.getAccountId() + " " + account.getAccountName();
          }
        }
      %>

      <!-- To modify account information do not connect to SQL source directly.  Make all changes
          through the admin page. -->

      <h1>Account Report - <%=accountName%></h1>

      <table width="590" border="0">
        <tr>
          <td colspan=2>
            <table cellSpacing="0" cellPadding="1" width="100%" border="1">
              <tr>
                <th colSpan="2">
                  Account Analysis</th></tr>
              <tr>
                <th align="left" width="80%" height="26">
                  <form id="Form1" method="get" action="showAccount">
                    <select size="1" name="listAccounts" id="listAccounts">
                      <%
                        for (Account account: accounts){
                          out.println("<option value=\""+account.getAccountId()+"\">" + account.getAccountId() + " " + account.getAccountName() + "</option>");
                        }
                        double dblBalance = Account.getAccount(paramName).getBalance();
                        String format = (dblBalance<1)?"$0.00":"$.00";
                        String balance = new DecimalFormat(format).format(dblBalance);
                      %>
                    </select>
                    <input type="submit" id="btnGetAccount" Value="Select Account">
                  </FORM>
                </th>
                <th align="middle" height="26">
                  Performance
                </th>
              </tr>
              <%
                ArrayList<Profile> profiles = DBUtil.getRiskRetHistory(Long.parseLong(paramName));
              %>
              <tr>
                <td>Annualized Return
                </td>
                <td align="right"><%=String.format("%.2f",Profile.calAvgRet(profiles))+"%"%></td>
              </tr>
              <tr>
                <td>Annualized Volatility
                </td>
                <td align="right">
                  <%=String.format("%.2f",Profile.calStd(profiles))+"%" %></td>
              </tr>
              <tr>
                <td>Sharpe Ratio
                </td>
                <td align="right">
                  <%=String.format("%.2f",Profile.calSharpeRatio(profiles))%></td>
              </tr>
            </table>
          </td>
        </tr>

        <tr>
          <td>
            <br><b>Daily return for the portfolio</b>
            <table border=1 cellpadding=2 cellspacing=0 width='600'>
              <tr>
                <th>Date</th>
                <th>Cash</th>
                <th>Asset Value</th>
                <th>Equity</th>
                <th>Profit(%)</th>
              </tr>
                <%
                  for (int i = profiles.size() - 1; i >= 0; i--) {
                    //String date = new SimpleDateFormat("yyyy-MM-dd").format(profile.date);
                %>
                <tr>
                  <td><%=profiles.get(i).date%></td>
                  <td><%=String.format("%.2f", profiles.get(i).cash)%></td>
                  <td><%=String.format("%.2f", profiles.get(i).asset)%></td>
                  <td><%=String.format("%.2f", profiles.get(i).equity)%></td>
                  <td><%=String.format("%.2f", profiles.get(i).pctPnL*100)+"%"%></td>
                </tr>
                <% } %>
              </table></DIV>
          </td>
        </tr>
      </table>

    </div>
  </td>
</div>

<jsp:include page="/footer.jspf"/>