<%@page import="com.ibm.security.appscan.bigbucks.model.Transaction"%>
<%@page import="com.ibm.security.appscan.bigbucks.util.DBUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%
/**
 This application is for demonstration use only. It contains known application security
vulnerabilities that were created expressly for demonstrating the functionality of
application security testing tools. These vulnerabilities may present risks to the
technical environment in which the application is installed. You must delete and
uninstall this demonstration application upon completion of the demonstration for
which it is intended. 

IBM DISCLAIMS ALL LIABILITY OF ANY KIND RESULTING FROM YOUR USE OF THE APPLICATION
OR YOUR FAILURE TO DELETE THE APPLICATION FROM YOUR ENVIRONMENT UPON COMPLETION OF
A DEMONSTRATION. IT IS YOUR RESPONSIBILITY TO DETERMINE IF THE PROGRAM IS APPROPRIATE
OR SAFE FOR YOUR TECHNICAL ENVIRONMENT. NEVER INSTALL THE APPLICATION IN A PRODUCTION
ENVIRONMENT. YOU ACKNOWLEDGE AND ACCEPT ALL RISKS ASSOCIATED WITH THE USE OF THE APPLICATION.

IBM AltoroJ
(c) Copyright IBM Corp. 2008, 2013 All Rights Reserved.
*/
%> 
    
<jsp:include page="/header.jspf"/>

<div id="wrapper" style="width: 99%;">
	<jsp:include page="membertoc.jspf"/>
    <td valign="top" colspan="3" class="bb">
		<%@page import="com.ibm.security.appscan.bigbucks.model.Account"%>
		<%@page import="java.text.SimpleDateFormat"%>
		<%@page import="java.text.NumberFormat"%>
		<%@page import="java.text.DecimalFormat"%>
		<%@page import="java.util.ArrayList"%>
		<%@ page import="com.ibm.security.appscan.bigbucks.model.Portfolio" %>
		<%@ page import="com.ibm.security.appscan.bigbucks.model.Stock" %>
		<div class="fl" style="width: 99%;">
		
		<%
					com.ibm.security.appscan.bigbucks.model.User user = (com.ibm.security.appscan.bigbucks.model.User)request.getSession().getAttribute("user");
					ArrayList<Account> accounts = new ArrayList<Account>();
					java.lang.String paramName = request.getParameter("acctId");
					String accountName = paramName;
													
					for (Account account: user.getAccounts()){
						
						if (!String.valueOf(account.getAccountId()).equals(paramName))
							accounts.add(account);
						else {
							accounts.add(0, account);
							accountName = account.getAccountId() + " " + account.getAccountName();
						}
					}
				%>
		
		<!-- To modify account information do not connect to SQL source directly.  Make all changes
		through the admin page. -->
		
		<h1>Account History - <%=accountName%></h1>
		
		<table width="590" border="0">
		  <tr>
		    <td colspan=2>
		      <table cellSpacing="0" cellPadding="1" width="100%" border="1">
		        <tr>
		          <th colSpan="2">
		            Balance Detail</th></tr>
		        <tr>
		          <th align="left" width="80%" height="26">
		            <form id="Form1" method="get" action="showAccount">
			           	<select size="1" name="listAccounts" id="listAccounts">
							<%
								for (Account account: accounts){
									out.println("<option value=\""+account.getAccountId()+"\">" + account.getAccountId() + " " + account.getAccountName() + "</option>");
								}
								double dblBalance = Account.getAccount(paramName).getBalance();
								String format = (dblBalance<1)?"$0.00":"$.00";
								String balance = new DecimalFormat(format).format(dblBalance);
							%>
						</select>
		           		<input type="submit" id="btnGetAccount" Value="Select Account">
		          </FORM>
		          </th>
		          <th align="middle" height="26">
		            Amount
		          </th>
		        </tr>
		        <tr>
		          <td>Ending balance as of <%= new SimpleDateFormat().format(new java.util.Date()) %>
		          </td>
		          <td align="right"><% out.println(balance); %></td>
		        </tr>
			        <tr>
		          <td>Available balance
		          </td>
		          <td align="right">
		            <% out.println(balance); %></td>
		        </tr>
		      </table>
		    </td>
		  </tr>
			<tr>
				<td>
					<a href=<%="riskreturnprofile.jsp?acctId="+paramName%>><button>View Risk-return Profile</button></a>
				</td>
				<td></td>
			</tr>
		  <tr>
		    <td>
		      <br><b>10 Most Recent Transactions</b>
				<table border=1 cellpadding=2 cellspacing=0 width='590'>
					<tr>
						<th bgcolor=#cccccc width=100>Date </th>
						<th width=290>Description</th>
						<th width=100>Amount</th></tr>
				</table>
				<DIV ID='recent' STYLE='overflow: hidden; overflow-y: scroll; width:590px; height: 152px; padding:0px; margin: 0px' >
					<table border=1 cellpadding=2 cellspacing=0 width='574'>
		      <%
		      Transaction[] transactions = DBUtil.getTransactions(null, null, new Account[]{DBUtil.getAccount(Long.valueOf(paramName))}, 10);
				for (Transaction transaction: transactions){		      
			   		double dblAmt = transaction.getAmount();
					String dollarFormat = (dblAmt<1)?"$0.00":"$.00";
					String amount = new DecimalFormat(dollarFormat).format(dblAmt);
					String date = new SimpleDateFormat("yyyy-MM-dd").format(transaction.getDate());
		      %>
		      <tr><td width=99><%=date%></td><td width=292><%=transaction.getTransactionType().substring(0,1).toUpperCase()+transaction.getTransactionType().substring(1)%> <%=transaction.getShare()%> share <%=transaction.getStockId()%></td><td width=84 align=right><%=amount%></td></tr>
		      <% } %>
		      </table></DIV>
		    </td>
		  </tr>
		  <tr>
		    <td>
		      <br><b>Portfolios</b>
				<table border=1 cellpadding=2 cellspacing=0 width='590'>
					<tr>
						<th width=70>Stock ID</th>
						<th width=290>Stock Name </th>
						<th width=70>Position</th>
						<th width=70>Buy-In Price</th>
					</tr>
				</table>
				<DIV ID='position' STYLE='overflow: hidden; overflow-y: scroll; width:590px; height: 152px; padding:0px; margin: 0px' >
					<table border=1 cellpadding=2 cellspacing=0 width='574'>
						<%
							Portfolio[] portfolios = DBUtil.getPortfolios(new Account[]{DBUtil.getAccount(Long.valueOf(paramName))});
							for (Portfolio portfolio: portfolios){
								double price = portfolio.getBuyinPrice();
								String dollarFormat = (price<1)?"$0.00":"$.00";
								String buyinPrice = new DecimalFormat(dollarFormat).format(price);
						%>
						<tr>
							<td width=63><%=portfolio.getStockId()%></td>
							<td width=310><%=Stock.getStock(portfolio.getStockId()).getStockName()%></td>
							<td width=63><%=portfolio.getShare()%></td>
							<td width=47><%=buyinPrice%></td>
						</tr>
						<% } %>
					</table>
				</DIV>
		    </td>
		  </tr>
		</table>
		
		</div>
    </td>	
</div>

<jsp:include page="/footer.jspf"/>  