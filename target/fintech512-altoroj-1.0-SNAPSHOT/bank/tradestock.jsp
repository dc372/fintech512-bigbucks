
<%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 3/17/22
  Time: 6:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<style>
    .buyBtn {
        background-color: #4CAF50; /* Green */
        border: none;
        border-radius: 12px;
        border: 1.5px solid #4CAF50;
        color: white;
        font-size: 15px;
        background: #73a405;
        display: inline-block;
        padding: 9px 25px 9px 25px;

        margin: 10px;

        -webkit-transition-duration: 0.4s; /* Safari */
        transition-duration: 0.4s;
    }

    .buyBtn:hover {
        background: white;
        color: black;
    }

    .sellBtn {
        background-color: red; /* Green */
        border: none;
        border-radius: 12px;
        border: 1.5px solid red;
        color: white;
        font-size: 15px;
        display: inline-block;
        padding: 9px 25px 9px 25px;

        margin: 10px;

        -webkit-transition-duration: 0.4s; /* Safari */
        transition-duration: 0.4s;
    }

    .sellBtn:hover {
        background: white;
        color: black;
    }
</style>

<jsp:include page="/header.jspf"/>

<div id="wrapper" style="width: 99%;">
    <jsp:include page="membertoc.jspf"/>
    <td valign="top" colspan="3" class="bb">
        <%@page import="com.ibm.security.appscan.bigbucks.model.Stock"%>
        <%@page import="java.text.SimpleDateFormat"%>
        <%@page import="java.text.NumberFormat"%>
        <%@page import="java.text.DecimalFormat"%>
        <%@page import="java.util.ArrayList"%>
        <%@ page import="com.ibm.security.appscan.bigbucks.model.Account" %>
        <%@ page import="com.ibm.security.appscan.bigbucks.util.APIUtil" %>
        <%@ page import="com.ibm.security.appscan.bigbucks.util.DBUtil" %>
        <%@ page import="java.awt.image.BufferedImage" %>
        <%@ page import="org.jfree.chart.ChartUtilities" %>
        <%@ page import="com.ibm.security.appscan.bigbucks.util.ChartUtil" %>
        <%@ page import="org.jfree.chart.servlet.ServletUtilities" %>
        <%@ page import="yahoofinance.YahooFinance" %>
        <%@ page import="java.math.BigDecimal" %>
        <div class="fl" style="width: 99%;">
            <%
                com.ibm.security.appscan.bigbucks.model.User user = (com.ibm.security.appscan.bigbucks.model.User)request.getSession().getAttribute("user");
            %>

        <%
            ArrayList<Stock> stocks = new ArrayList<Stock>();
            java.lang.String paramName = request.getParameter("stockId");
            String stockName = paramName;

            for (Stock stock: Stock.getStocks()) {
                if (!String.valueOf(stock.getStockId()).equals(paramName))
                    stocks.add(stock);
                else {
                    stocks.add(0, stock);
                    stockName = "(" + stock.getStockId() + ") " +stock.getStockName();
                }
            }
        %>

        <!-- To modify account information do not connect to SQL source directly.  Make all changes
        through the admin page. -->

        <h1>Stock Detail - <%=stockName%></h1>

        <table width="700" border="0">
            <tr>
                <td colspan=2>
                    <table cellSpacing="0" cellPadding="1" width="100%" border="1">
                        <tr>
                            <th colSpan="2" align="left">
                                Balance Detail</th></tr>
                        <tr>
                            <th align="left" width="80%" height="26">
                                <form id="Form1" method="get" action="showStock">
                                    <select size="1" name="listStocks" id="listStocks">
                                        <%
                                            for (Stock stock: stocks){
                                                out.println("<option value=\""+stock.getStockId()+"\">(" + stock.getStockId() + ") " + stock.getStockName() + "</option>");
                                            }
                                            double dblPrice = APIUtil.getLastAdjClose(paramName);
                                            String closeDate = DBUtil.getLastQuoteDatetime(paramName);

                                            yahoofinance.Stock ystock = YahooFinance.get(paramName);
                                            BigDecimal buyPrice = ystock.getQuote().getAsk(); //Ask is the min price a seller want to sell
                                            BigDecimal sellPrice = ystock.getQuote().getBid();
                                            //double[] priceArr = APIUtil.getLive(paramName);
                                            //double sellPrice = priceArr[0];
                                            //double sellPrice = 1;
                                            //double buyPrice = priceArr[1];
                                            //double buyPrice = 1;
                                            String format = "$0.00";
                                            String sellPriceString = new DecimalFormat(format).format(sellPrice);
                                            String buyPriceString = new DecimalFormat(format).format(buyPrice);
                                        %>
                                    </select>
                                    <input type="submit" id="btnGetStock" Value="Select Stock">
                                </FORM>
                            </th>
                            <th align="right" height="26">
                                Price
                            </th>
                        </tr>
                        <tr>
                            <td>Adjusted Closed Price as of <%=closeDate %>
                            </td>
                            <td align="right"><% out.println(new DecimalFormat(format).format(dblPrice)); %></td>
                        </tr>
                        <tr>
                            <td>Live bid price
                            </td>
                            <td align="right" value="<%=sellPrice%>" id="LiveBidPrice">
                                <%=sellPriceString%> x <%=ystock.getQuote().getBidSize()*100%></td>
                        </tr>
                        <tr>
                            <td>Live ask price
                            </td>
                            <td align="right">
                                <%=buyPriceString%> x <%=ystock.getQuote().getAskSize()*100%></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


                <p>
                <%
                    String error = "";
                    String filename = null;
                    try {
                        filename = ServletUtilities.saveChartAsPNG(ChartUtil.createChart(paramName), 690, 400, null);

                    } catch (Exception e) {
                        error = e.getMessage() +"::"+ e.getStackTrace()[2].getFileName()+"::"+e.getStackTrace()[2].getLineNumber() +"::" + e.getStackTrace()[3].getFileName()+"::"+e.getStackTrace()[3].getLineNumber();
                    }
                    String graphURL = request.getContextPath() + "/servlet/DisplayChart?filename="   + filename;
                %>

                    <b>
                        click <a href="chartdetail.jsp?stockId=<%=paramName%>">here</a> to view more detailed chart analysis.
                    </b>
                        <%=error%>
                    <br>
                <img src="<%=graphURL%>" border="0" alt="<%=error%>" usemap="#<%=filename %>"/>
                <br>



                <button class="buyBtn" id="buyBtn" onclick="clickBtn('Buy');" >Buy</button><button class="sellBtn" id="sellBtn" onclick="clickBtn('Sell');">Sell</button>

        <p style="display: block" id="message"><span style="color:#FF0066;font-size:12pt;font-weight:bold;">
                <%
                    java.lang.String trade_message = (String)request.getSession().getAttribute("message");

                    if (trade_message != null && trade_message.trim().length() > 0){
                        request.getSession().removeAttribute("message");
                        out.print(trade_message);
                    }
                %>
           </span></p>


        <script>
            function clickBtn(tradeType){
                var myDate = new Date();

                if (myDate.getHours() >= 20 || myDate.getHours() <9 || (myDate.getHours() === 9 && myDate.getMinutes() <= 30) )
                {
                    alert("Not In Trading time");
                    return;
                }

                if (document.getElementById("LiveBidPrice").getAttribute("value") <= 0) {
                    alert("Cannot trade because of no counterparty");
                    return;
                }

                var message_block = document.getElementById("message").style
                if (message_block.display === "block")
                    message_block.display = "none"

                const table = document.getElementById("tradeInformation");
                const typeElement = document.getElementById("tradeBtn")
                if(table.style.display==="none") {
                    table.style.display="block"
                    //typeElement.innerText=tradeType
                    document.getElementById("titleBuy").innerText="Share to "+tradeType.lowercase()
                    typeElement.value=tradeType
                }

                else if(table.style.display==="block" && typeElement.value===tradeType) {
                    table.style.display = "none"
                    //typeElement.innerText = tradeType
                }

                else {
                    //typeElement.innerText = tradeType
                    document.getElementById("titleBuy").innerText="Share to "+tradeType
                    typeElement.value=tradeType
                }

                /*var priceElement = document.getElementById("price")
                if (tradeType=='Buy') {
                    priceElement.value=<%=buyPrice%>;
                    priceElement.innerText=<%=buyPriceString%>;
                    document.getElementById("priceSubmit").value=<%=buyPrice%>;
                } else {
                    riceElement.value=<%=sellPrice%>;
                    priceElement.innerText=<%=sellPriceString%>;
                    document.getElementById("priceSubmit").value=<%=sellPrice%>;
                }*/
            }
        </script>

        <li id="tradeInformation" style="display:none;" >
            <form action="tradeStock" method="post" name="tradeStock" id="tradeStock">
            <table width="700" border="0" >
                <tr>
                    <td> <b style="font-size:10pt;"> Operating Account: </b>

                    <select size="1" id="fromAccount" name="fromAccount">
                        <%
                            for (Account account: user.getAccounts()){
                                out.println("<option value=\""+account.getAccountId()+"\" >" + account.getAccountId() + " " + account.getAccountName() + "</option>");
                            }
                        %>
                    </select>
                    </td>
                </tr>
                <tr>
                    <td colspan=2>
                        <table cellSpacing="0"  width="100%" border="1">
                            <tr>
                                <th width="25%" class="font-center">
                                    Market Price
                                </th>
                                <th width="25%" class="font-center" id="titleBuy">
                                    Share to buy
                                </th>
                                <th width="25%" class="font-center">
                                    Amount
                                </th>
                                <th width="25%" class="font-center">
                                    Trade type
                                </th>
                            </tr>
                            <tr>
                                <td id="price" value=<%=sellPrice%>>
                                    <%=sellPriceString%>
                                </td>
                                <td>
                                    <input type="number" id="share" name="share" value="100" oninput="this.value = this.value.replace(/[^0-9]/g, '');" min="1">
                                </td>
                                <td id="amount">
                                    <script>
                                        function fmoney(s, n) {
                                            n = n > 0 && n <= 20 ? n : 2;
                                            s = parseFloat((s + "").replace(/[^\d\.-]/g, "")).toFixed(n) + "";
                                            var l = s.split(".")[0].split("").reverse(),
                                                r = s.split(".")[1];
                                            t = "";
                                            for (i = 0; i < l.length; i++) {
                                                t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
                                            }
                                            return t.split("").reverse().join("") + "." + r;
                                        }

                                        const share = parseFloat(document.getElementById("share").value)
                                        const price = parseFloat(<%=sellPrice%>)
                                        document.getElementById("amount").innerText="$"+String(fmoney(share*price,2))

                                        const number = document.getElementById("share");
                                        number.addEventListener("input", function () {
                                            let share = parseFloat(document.getElementById("share").value)
                                            if (!share) {
                                                share = 0
                                            }
                                            document.getElementById("amount").innerText="$"+String(fmoney(share*price,2))
                                        })
                                    </script>
                                </td>
                                <td>
                                    <input type="hidden" value=<%=paramName%> name="stockId">
                                    <input type="hidden" value=<%=sellPrice%> id="priceSubmit" name="priceSubmit">
                                    <input type="submit" value="Buy" name="trade" id="tradeBtn">
                                </td>
                            </tr>
                        </table>
                        </form>
                    </td>
                </tr>
            </table>
            <form action="tradeStock" method="post" name="tradeStock" id="tradeStock">
        </li>

    </div>
    </td>
</div>

<jsp:include page="/footer.jspf"/>