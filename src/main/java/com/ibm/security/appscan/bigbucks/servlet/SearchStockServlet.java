package com.ibm.security.appscan.bigbucks.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SearchStockServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getRequestURL().toString().endsWith("searchStock")){
            String searchStr = request.getParameter("wd");
            if (searchStr == null || searchStr.length()==0) {
                response.sendRedirect(request.getContextPath()+"/bank/pickstock.jsp");
                return;
            }
            RequestDispatcher dispatcher = request.getRequestDispatcher("/bank/pickstock.jsp?wd=" + searchStr);
            dispatcher.forward(request, response);
            return;
        }
    }
}
