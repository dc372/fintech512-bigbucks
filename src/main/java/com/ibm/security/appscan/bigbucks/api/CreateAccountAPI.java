package com.ibm.security.appscan.bigbucks.api;

import com.ibm.security.appscan.bigbucks.util.DBUtil;
import org.apache.wink.json4j.JSONException;
import org.apache.wink.json4j.JSONObject;

import javax.annotation.security.PermitAll;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.sql.SQLException;

@Path("/createAccount")
public class CreateAccountAPI extends BigBucksAPI{


    @POST
    @PermitAll
    public Response addUser(String bodyJSON, @Context HttpServletRequest request) throws IOException, SQLException {
        JSONObject bodyJson= new JSONObject();

        //Checking if user is logged in

        String firstname;
        String lastname;
        String username;
        String password1;
        String password2;

        //Convert request to JSON
        try {
            bodyJson =new JSONObject(bodyJSON);
            //Parse the request for the required parameters
            firstname = bodyJson.get("firstname").toString();
            lastname = bodyJson.get("lastname").toString();
            username = bodyJson.get("username").toString();
            password1 = bodyJson.get("password1").toString();
            password2 = bodyJson.get("password2").toString();
        } catch (JSONException e) {
            return Response.status(Response.Status.BAD_REQUEST).entity("{\"Error\": \"Request is not in JSON format\"}").build();
        }

        if (username == null || username.trim().length() == 0
                || password1 == null || password1.trim().length() == 0
                || password2 == null || password2.trim().length() == 0)
            return Response.status(Response.Status.BAD_REQUEST).entity("{\"error\":\"An error has occurred. Please try again later.\"}").build();

        if (!password1.equals(password2)){
            return Response.status(Response.Status.BAD_REQUEST).entity("{\"error\":\"Entered passwords did not match.\"}").build();
        }

        String error = DBUtil.createAccount(firstname, lastname, username, password1);

        if (error != null)
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("{\"error\":\""+error+"\"}").build();


        return Response.status(Response.Status.OK).entity("{\"success\":\"Requested operation has completed successfully.\"}").type(MediaType.APPLICATION_JSON_TYPE).build();
    }

}
