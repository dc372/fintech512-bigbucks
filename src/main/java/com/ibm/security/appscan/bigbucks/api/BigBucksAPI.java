package com.ibm.security.appscan.bigbucks.api;


import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

import com.ibm.security.appscan.bigbucks.filter.ApiAuthFilter;

@ApplicationPath("api")
public class BigBucksAPI extends ResourceConfig {
	public BigBucksAPI(){
		packages("com.ibm.security.appscan.altoromutual.api");
		register(ApiAuthFilter.class);
	}
}
