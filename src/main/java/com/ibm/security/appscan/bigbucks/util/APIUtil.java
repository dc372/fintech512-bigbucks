package com.ibm.security.appscan.bigbucks.util;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ibm.security.appscan.bigbucks.model.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class APIUtil {
    //public static String apiKey = "oPwTRVzOcB5xxDM81pLiO39lErkkvtcJ1DX9SebZ";
    //public static String apiKey = "0Mq7QXQbm4310EhSxPCx6o9w5Jp1yT643uO0YCn9";
    public static String apiKey = "R8VbahQWfF1z4rurN7RbV4J6WqbFmpq46VNWmcY4";
    //public static String apiKey = "gOT0bVdp7f5NpMjPLdBXA3pQer95MNWF51b3c2SG";
    //public static String apiKey = "FxhV1D9ovN7oLcBTRziF86aBXR8AgtVw1d5AkBzt";


    /**
     * get live market data URL
     * @param symbol
     * @return
     */
    public static String _getQuoteURI(String symbol){
        return "https://yfapi.net/v6/finance/quote?region=US&lang=en&symbols=" + symbol;
    }

    /**
     * get historical data URL
     * @param symbol
     * @param range
     * @param interval
     * @return
     */
    public static String _getChartURI(String symbol, String range, String interval){
        return "https://yfapi.net/v8/finance/chart/" + symbol + "?range=" + range + "&region=US" +
                "&interval=" + interval + "&lang=en";
    }

    public static String _getSearchURI(String searchStr) {
        return "https://yfapi.net/v6/finance/autocomplete?region=US&lang=en&query=" + searchStr;
    }

    /**
     * send request
     * @param uri
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public static String _requestData(String uri) throws IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder().uri(URI.create(uri))
                .header("x-api-key", apiKey)
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

//    public static String _fetchLive(String symbol) throws IOException, InterruptedException{
//        String uri = _getQuoteURI(symbol);
//        return  _requestData(uri);
//    }
//
//    public static String _fetchHistorical(String symbol, String range) throws IOException, InterruptedException {
//        String uri = _getChartURI(symbol.replaceAll("\\^","%5E"), range, "1d");
//        return _requestData(uri);
//    }

    public static String _fetchSearch(String searchStr) throws IOException, InterruptedException {
        String uri = _getSearchURI(searchStr);
        return _requestData(uri);
    }

    public static ArrayList<Stock> getSearch(String searchStr) throws IOException, InterruptedException {
        String response = _fetchSearch(searchStr);
        JSONArray result = JSONObject.parseObject(response).getJSONObject("ResultSet").getJSONArray("Result");

        ArrayList<Stock> stockLists = new ArrayList<Stock>();
        for (int i=0; i<result.size(); ++i) {
            JSONObject info = result.getJSONObject(i);
            //System.out.println(info.getString("type"));
            if (!info.getString("type").equals("S")) {
                continue;
            }
            String exchDisp = info.getString("exchDisp");
            if (!exchDisp.equals("NASDAQ") && !exchDisp.equals("NYSE")) {
                continue;
            }
            String symbol =  info.getString("symbol");
            String name = info.getString("name");
            stockLists.add(new Stock(symbol, name));
            DBUtil.addStock(symbol, name);
        }

        return stockLists;
    }

//    public static void _setHistorical(String response, String symbol) throws SQLException {
//        JSONObject resultObj = JSONObject.parseObject(response).getJSONObject("chart")
//                .getJSONArray("result").getJSONObject(0);
//
//        JSONObject quoteObj = resultObj.getJSONObject("indicators").getJSONArray("quote")
//                .getJSONObject(0);
//
//        JSONObject adjCloseObj = resultObj.getJSONObject("indicators").getJSONArray("adjclose")
//                .getJSONObject(0);
//
//        ArrayList<Long> timestamp = _JSONArrayToLongList(resultObj.getJSONArray("timestamp"));
//        ArrayList<Double> low = _JSONArrayToDoubleList(quoteObj.getJSONArray("low"));
//        ArrayList<Double> volume = _JSONArrayToDoubleList(quoteObj.getJSONArray("volume"));
//        ArrayList<Double> open = _JSONArrayToDoubleList(quoteObj.getJSONArray("open"));
//        ArrayList<Double> high = _JSONArrayToDoubleList(quoteObj.getJSONArray("high"));
//        ArrayList<Double> close = _JSONArrayToDoubleList(quoteObj.getJSONArray("close"));
//        ArrayList<Double> adjClose = _JSONArrayToDoubleList(adjCloseObj.getJSONArray("adjclose"));
//
//        DBUtil.setQuote(symbol, timestamp, open, high, low, close, volume, adjClose);
//    }

    public static void _setHistorical(String symbol, String time) throws SQLException, IOException {
        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        int amount = Integer.parseInt("-" + Integer.parseInt(time.substring(0, time.length() - 1)));
        if (time.endsWith("y")) {
            from.add(Calendar.YEAR, amount);
        }
        else if (time.endsWith("d")) {
            from.add(Calendar.DATE, amount);
        }

        yahoofinance.Stock stock = YahooFinance.get(symbol);

        List<HistoricalQuote> history = stock.getHistory(from, to, Interval.DAILY);

        DBUtil.setQuote(symbol, history);
    }

    /**
     *
     * @param symbol
     * @param columnName
     * @return historical data for 5 years of specific stock
     * @throws IOException
     * @throws InterruptedException
     * @throws SQLException
     * @throws ParseException
     */

    public static ArrayList<String> getHistorical(String symbol, String columnName) throws IOException,
            InterruptedException, SQLException, ParseException {
        /*
         * This method returns historical data for at least five years.
         * @params: string symbol: stock symbol
         * @params: string columnName: columnName of the local database QUOTE.
         * Valid columnName: DATETIME, PRICE_OPEN, PRICE_HIGH, PRICE_LOW, PRICE_CLOSE,
         *                   PRICE_ADJ_CLOSE, VOLUME, SIMPLE_RETURN
         * @return a list of historical data, from the earliest to the latest. Call
         * getHistorical(symbol, "DATETIME") to get detailed datetime for each datum.
         */
        String lastDate = DBUtil.getLastQuoteDatetime(symbol);
        String today = _getToday();
        String yesterday = _getYesterday();
        String setDate = (_getHour()>16)?today:yesterday;
        if (lastDate.equals("null")){
            // *****************************************************************
            // change to 5y in production
            //String response = _fetchHistorical(symbol, "5y");
            // *****************************************************************
            //_setHistorical(response, symbol);
            _setHistorical(symbol,"5y");
            return DBUtil.getQuote(symbol, columnName);
        }
        else if (!lastDate.equals(setDate)){
            long daysBetween = _countDays(lastDate, setDate) + 1;
            //String response = _fetchHistorical(symbol, daysBetween + "d");
            _setHistorical(symbol,daysBetween+"d");
        }
        return DBUtil.getQuote(symbol, columnName);
    }

    public static double getLastAdjClose(String symbol) throws SQLException, ParseException, IOException, InterruptedException {
        String lastDate = DBUtil.getLastQuoteDatetime(symbol);
        String today = _getToday();
        String yesterday = _getYesterday();
        String setDate = (_getHour()>16)?today:yesterday;
        if (lastDate.equals("null")){
            // *****************************************************************
            // change to 5y in production
            //String response = _fetchHistorical(symbol, "5y");
            // *****************************************************************
            _setHistorical(symbol,"5y");
            return DBUtil.getLastAdjClose(symbol);
        }
        else if (!lastDate.equals(setDate)){
            long daysBetween = _countDays(lastDate, setDate);
            //String response = _fetchHistorical(symbol, daysBetween + "d");
            _setHistorical(symbol, daysBetween + "d");
        }
        return DBUtil.getLastAdjClose(symbol);
    }

//    public static double[] getLive(String symbol) throws IOException, InterruptedException {
//        String response = _fetchLive(symbol);
//        double LiveBuy = JSONObject.parseObject(response)
//                .getJSONObject("quoteResponse")
//                .getJSONArray("result")
//                .getJSONObject(0).getDouble("bid");
//        double LiveSell = JSONObject.parseObject(response)
//                .getJSONObject("quoteResponse")
//                .getJSONArray("result")
//                .getJSONObject(0).getDouble("ask");
//        if (new Double(LiveBuy) == null) {
//            LiveBuy = 0;
//        }
//        if (new Double(LiveSell) == null) {
//            LiveSell = 0;
//        }
//        return new double[]{LiveBuy, LiveSell};
//    }

    /**
     * auxiliary functions for date
     * @param start
     * @param end
     * @return
     * @throws ParseException
     */
    public static long _countDays(String start, String end) throws ParseException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime date1 = LocalDateTime.parse(start+" 00:00:00", dtf);
        LocalDateTime date2 = LocalDateTime.parse(end+" 00:00:00", dtf);
        return Duration.between(date1, date2).toDays();
    }

    public static String _getToday(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static int _getHour() {
        DateFormat hourFormat = new SimpleDateFormat("HH");
        java.util.Date date = new java.util.Date();
        return Integer.parseInt(hourFormat.format(date));
    }

    public static int _getMinute() {
        DateFormat hourFormat = new SimpleDateFormat("MM");
        java.util.Date date = new java.util.Date();
        return Integer.parseInt(hourFormat.format(date));
    }

    public static String _getYesterday() {
        Calendar cal   =   Calendar.getInstance();
        cal.add(Calendar.DATE,   -1);
        return new SimpleDateFormat( "yyyy-MM-dd").format(cal.getTime());
    }

    /**
     * JSON Method
     * @param jsonArray
     * @return
     */
    public static ArrayList<Double> _JSONArrayToDoubleList(JSONArray jsonArray){
        ArrayList<Double> listData = new ArrayList<>();
        if (jsonArray != null) {
            for (int i=0; i < jsonArray.size(); i++){
                listData.add(jsonArray.getDouble(i));
            }
        }
        return listData;
    }

    public static ArrayList<Long> _JSONArrayToLongList(JSONArray jsonArray){
        ArrayList<Long> listData = new ArrayList<>();
        if (jsonArray != null) {
            for (int i=0; i < jsonArray.size(); i++){
                listData.add(jsonArray.getLong(i));
            }
        }
        return listData;
    }

    public static void main(String[] args) throws SQLException, IOException, ParseException, InterruptedException {
        System.out.println(getHistorical("MSFT", "price_adj_close"));
        System.out.println(getHistorical("MSFT", "datetime"));
        System.out.println(getHistorical("BABA","price_adj_close"));
        System.out.println(getHistorical("BABA", "datetime"));
    }
}
