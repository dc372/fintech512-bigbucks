package com.ibm.security.appscan.bigbucks.model;

import com.ibm.security.appscan.bigbucks.util.DBUtil;

import java.sql.SQLException;

/**
 * This class models stock
 * @author Dongchen
 */
public class Stock {
    private String stockId = null;
    private String stockName = null;
    //private double price = -1;

    public static Stock getStock(String stockNo) throws SQLException {
        return DBUtil.getStock(stockNo);
    }

    public static Stock[] getStocks() {
        try {
            return DBUtil.getStocks();
        } catch (SQLException e) {
            e.printStackTrace();
            Stock nullStock = new Stock(e.getLocalizedMessage(), "null");
            return new Stock[]{nullStock};
        }
    }

    public Stock(String stockId, String stockName) {
        this.stockId = stockId;
        this.stockName = stockName;
        //this.price = price;
    }

    public String getStockId() { return stockId; }

    public void setStockId(String stockId) { this.stockId = stockId; }

    //public double getPrice() { return price; }

    //public void setPrice(double price) { this.price = price; }

    public String getStockName() { return stockName; }
}
