package com.ibm.security.appscan.bigbucks.model;

import com.ibm.security.appscan.bigbucks.util.DBUtil;
import yahoofinance.YahooFinance;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;

public class Profile {
    public LocalDate date;
    public double cash;
    public double asset;
    public double equity;
    public double PnL;
    public double pctPnL;

    public Profile(LocalDate date, double cash, double asset, double equity, double PnL, double pctPnL) {
        this.date = date;
        this.cash = cash;
        this.asset = asset;
        this.equity = equity;
        this.PnL = PnL;
        this.pctPnL = pctPnL;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public void setAsset(double asset) {
        this.asset = asset;
    }

    public void setEquity(double equity) {
        this.equity = equity;
    }

    public void setPctPnL(double pctPnL) {
        this.pctPnL = pctPnL;
    }

    public void setPnL(double pnL) {
        this.PnL = pnL;
    }

    public static double calAvgRet(ArrayList<Profile> profile){
        double retsum = 0;
        for(int i=0;i<profile.size();i++){
            retsum = retsum + profile.get(i).pctPnL;
        }
        double avgret = retsum/profile.size();
        return avgret;
    }

    public static double calStd(ArrayList<Profile> profile){
        double stdsum = 0;
        double avgret = calAvgRet(profile);
        for(int i=0;i<profile.size();i++){
            stdsum = stdsum + (profile.get(i).pctPnL-avgret)*(profile.get(i).pctPnL-avgret);
        }
        double std = Math.sqrt(stdsum/profile.size());
        return std;
    }

    public static double calSharpeRatio(ArrayList<Profile> profile) throws IOException {
        double rf = YahooFinance.get("^TNX").getQuote().getPreviousClose().doubleValue()/100;
        double avgret = calAvgRet(profile)*252;
        double std = calStd(profile)*Math.sqrt(252);
        double sharpeRatio = (avgret-rf)/std;
        return sharpeRatio;


    }
    public static void main(String[] args) throws SQLException, IOException, ParseException, InterruptedException {
        ArrayList<Profile> rr =  DBUtil.getRiskRetHistory(800000);
        for (int i=0;i<rr.size();i++){
            System.out.println(rr.get(i));
        }
        System.out.println(calAvgRet(rr)*252);
        System.out.println(calStd(rr)*Math.sqrt(252));
        System.out.println(calSharpeRatio(rr));
    }
}
