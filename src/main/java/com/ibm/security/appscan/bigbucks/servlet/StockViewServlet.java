package com.ibm.security.appscan.bigbucks.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class StockViewServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //show account balance for a particular account
        if (request.getRequestURL().toString().endsWith("showStock")){
            String stockName = request.getParameter("listStocks");
            if (stockName == null){
                response.sendRedirect(request.getContextPath()+"/stockprice.jsp");
                return;
            }
//			response.sendRedirect("/bank/balance.jsp&acctId=" + accountName);

//            String filename = null;
//            try {
//                filename = ServletUtilities.saveChartAsPNG(ChartUtil.createChart(stockName), 500, 270, null);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            String graphURL = request.getContextPath() + "/DisplayChart?filename="   + filename;
//            String image=  "<img src='"
//                    + graphURL
//                    + "' width=600 height=400 border=0 usemap='#"
//                    + filename + "'/>";
//            request.setAttribute("image", image);

            RequestDispatcher dispatcher = request.getRequestDispatcher("/stockdetail.jsp?stockId=" + stockName);
            dispatcher.forward(request, response);
            return;
        }
    }
}
