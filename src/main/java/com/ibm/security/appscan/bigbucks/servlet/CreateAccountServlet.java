package com.ibm.security.appscan.bigbucks.servlet;

import com.ibm.security.appscan.bigbucks.util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CreateAccountServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.sendRedirect("createAccount.jsp");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = null;

        //create account
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String username = request.getParameter("username");
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");
        if (username == null || username.trim().length() == 0
                || password1 == null || password1.trim().length() == 0
                || password2 == null || password2.trim().length() == 0)
            message = "An error has occurred. Please try again later.";

        if (firstname == null){
            firstname = "";
        }

        if (lastname == null){
            lastname = "";
        }

        if (message == null && !password1.equals(password2)){
            message = "Entered passwords did not match.";
        }

        if (message == null){
            String error = null;
            error = DBUtil.createAccount(firstname.toLowerCase(), lastname.toLowerCase(), username.toLowerCase(), password1.toLowerCase());

            if (error != null)
                message = error;
        }

        if (message == null){
            message = "Create account success! Welcome to Big Bucks.";
            request.getSession().setAttribute("message", message);
            response.sendRedirect("login.jsp");
        }

        else{
            request.getSession().setAttribute("message", message);
            response.sendRedirect("createAccount.jsp");
        }
    }
}

