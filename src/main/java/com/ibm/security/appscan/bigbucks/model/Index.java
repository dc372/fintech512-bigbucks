package com.ibm.security.appscan.bigbucks.model;

import com.ibm.security.appscan.bigbucks.util.DBUtil;

import java.sql.SQLException;

public class Index {
    private String indexId = null;
    private String indexName = null;

    public static Index getIndex(String indexNo) throws SQLException {
        return DBUtil.getIndex(indexNo);
    }

    public static Index[] getIndexes() {
        try {
            return DBUtil.getIndexes();
        } catch (SQLException e) {
            e.printStackTrace();
            Index nullIndex = new Index(e.getLocalizedMessage(), "null");
            return new Index[]{nullIndex};
        }
    }

    public Index(String indexId, String indexName) {
        this.indexId = indexId;
        this.indexName = indexName;
    }

    public String getIndexId() { return indexId; }

    public String getIndexName() { return indexName; }
}
