package com.ibm.security.appscan.bigbucks.model;

public class TransactionSummary {

    private String stockId;
    private int buyShare;
    private int sellShare;

    public TransactionSummary(String stockId, int buyShare, int sellShare) {
        this.stockId = stockId;
        this.buyShare = buyShare;
        this.sellShare = sellShare;
    }

    public String getStockId() { return stockId; }

    public int getBuyShare() { return buyShare; }

    public int getSellShare() { return sellShare; }

    public void setBuyShare(int buyShare) { this.buyShare = buyShare; }

    public void setSellShare(int sellShare) { this.sellShare = sellShare; }

}
