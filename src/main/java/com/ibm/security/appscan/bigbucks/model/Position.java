package com.ibm.security.appscan.bigbucks.model;

import com.ibm.security.appscan.bigbucks.util.DBUtil;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.time.LocalDate;

public class Position {
    public LocalDate date;
    public String symbol;
    public long accountId;
    public int sharechg;
    public double amount;
    public double filledprice;
    public double marketprice;
    public double cashchg;
    public double assetchg;
    public double UnrealizedPnL;
    public double share;
    private static final DecimalFormat df = new DecimalFormat("0.00");

    public Position(LocalDate date, String symbol, long accountId, int sharechg, double amount,
                    double filledprice, double marketprice, double cashchq, double assetchg, double UnrealizedPnL, double share) {
        this.date = date;
        this.symbol = symbol;
        this.accountId = accountId;
        this.sharechg = sharechg;
        this.amount = amount;
        this.filledprice = filledprice;
        this.marketprice = marketprice;
        this.cashchg = cashchq;
        this.assetchg = assetchg;
        this.UnrealizedPnL = UnrealizedPnL;
        this.share = share;
    }

    public static void main(String[] args) throws SQLException, IOException, ParseException, InterruptedException {

        //DBUtil.getOrderFromTransaction(800200, "TSM");
        DBUtil.getRiskRetHistory(800200);
        //DBUtil.getAllUsersRiskRetHistory();
    }

}