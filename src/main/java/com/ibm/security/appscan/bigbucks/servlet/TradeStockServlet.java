package com.ibm.security.appscan.bigbucks.servlet;

import com.ibm.security.appscan.bigbucks.util.OperationsUtil;
import com.ibm.security.appscan.bigbucks.util.ServletUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

public class TradeStockServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /*@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doPost(req, resp);
    }*/

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        if(!ServletUtil.isLoggedin(request)){
            response.sendRedirect("login.jsp");
            return ;
        }

        String accountIdString = request.getParameter("fromAccount");
        String stockId = request.getParameter("stockId");
        String tradeType = request.getParameter("trade");
        int share = Integer.parseInt(request.getParameter("share"));
        double price = Double.parseDouble(request.getParameter("priceSubmit"));

        BigDecimal b = new BigDecimal(share*price);
        double amount = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();

        //DecimalFormat decimalFormat = new DecimalFormat("###,###.00");
        //String amountOutput = decimalFormat.format(amount);

        String message = OperationsUtil.doServletTrade(request,accountIdString,tradeType,stockId,share,price,amount);
        //String message = "Successfully " + tradeType + " " + share + " share stocks of " + stockId + ", trading amount: $" + amountOutput +".";


        request.getSession().setAttribute("message", message);
        response.sendRedirect("tradestock.jsp?stockId=" + stockId);

    }
}
