package com.ibm.security.appscan.bigbucks.model;

/**
 * This class models a Portfolios
 * @author Dongchen Chu
 *
 */
public class Portfolio {

    private int portfolioId;
    private long accountId;
    private String stockId;
    private int share;
    private double buyinPrice;

    public Portfolio(int portfolioId, long accountId, String stockId, int share, double buyinPrice) {
        this.portfolioId = portfolioId;
        this.accountId = accountId;
        this.stockId = stockId;
        this.share = share;
        this.buyinPrice = buyinPrice;
    }

    public int getPortfolioId() { return portfolioId; }

    public long getAccountId() { return accountId; }

    public String getStockId() { return stockId; }

    public int getShare() { return share; }

    public double getBuyinPrice() { return buyinPrice; }

    public void setShare(int share) { this.share = share; }

    public void setBuyinPrice(double buyinPrice) { this.buyinPrice = buyinPrice; }

}
