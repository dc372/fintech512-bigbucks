/**
 This application is for demonstration use only. It contains known application security
vulnerabilities that were created expressly for demonstrating the functionality of
application security testing tools. These vulnerabilities may present risks to the
technical environment in which the application is installed. You must delete and
uninstall this demonstration application upon completion of the demonstration for
which it is intended. 

IBM DISCLAIMS ALL LIABILITY OF ANY KIND RESULTING FROM YOUR USE OF THE APPLICATION
OR YOUR FAILURE TO DELETE THE APPLICATION FROM YOUR ENVIRONMENT UPON COMPLETION OF
A DEMONSTRATION. IT IS YOUR RESPONSIBILITY TO DETERMINE IF THE PROGRAM IS APPROPRIATE
OR SAFE FOR YOUR TECHNICAL ENVIRONMENT. NEVER INSTALL THE APPLICATION IN A PRODUCTION
ENVIRONMENT. YOU ACKNOWLEDGE AND ACCEPT ALL RISKS ASSOCIATED WITH THE USE OF THE APPLICATION.

IBM AltoroJ
(c) Copyright IBM Corp. 2008, 2013 All Rights Reserved.
 */

package com.ibm.security.appscan.bigbucks.util;

import com.ibm.security.appscan.Log4BigBucks;
import com.ibm.security.appscan.bigbucks.model.*;
import com.ibm.security.appscan.bigbucks.model.User.Role;
import yahoofinance.histquotes.HistoricalQuote;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Utility class for database operations
 * @author Alexei Dongchen
 *
 */
public class DBUtil {

	private static final String PROTOCOL = "jdbc:derby:";
	private static final String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
	
	public static final String CREDIT_CARD_ACCOUNT_NAME = "Credit Card";
	public static final String CHECKING_ACCOUNT_NAME = "Checking";
	public static final String SAVINGS_ACCOUNT_NAME = "Savings";
	
	public static final double CASH_ADVANCE_FEE = 2.50;
	
	private static DBUtil instance = null;
	private Connection connection = null;
	private DataSource dataSource = null;

	public static double init_balance = 1000000;
	
	//private constructor
	private DBUtil(){
		/*
**
**			Default location for the database is current directory:
**			System.out.println(System.getProperty("user.home"));
**			to change DB location, set derby.system.home property:
**			System.setProperty("derby.system.home", "[new_DB_location]");
**
		*/
		
		String dataSourceName = ServletUtil.getAppProperty("database.alternateDataSource");
		
		/* Connect to an external database (e.g. DB2) */
		if (dataSourceName != null && dataSourceName.trim().length() > 0){
			try {
				Context initialContext = new InitialContext();
				Context environmentContext = (Context) initialContext.lookup("java:comp/env");
				dataSource = (DataSource)environmentContext.lookup(dataSourceName.trim());
			} catch (Exception e) {
				e.printStackTrace();
				Log4BigBucks.getInstance().logError(e.getMessage());
			}
			
		/* Initialize connection to the integrated Apache Derby DB*/	
		} else {
			System.setProperty("derby.system.home", System.getProperty("user.home")+"/bigbucks/");
			System.out.println("Derby Home=" + System.getProperty("derby.system.home"));
			
			try {
				//load JDBC driver
				Class.forName(DRIVER).newInstance();
			} catch (Exception e) {
				Log4BigBucks.getInstance().logError(e.getMessage());
				e.printStackTrace();
			}
		}
	}

	private static Connection getConnection() throws SQLException{

		if (instance == null)
			instance = new DBUtil();
		
		if (instance.connection == null || instance.connection.isClosed()){
			
			//If there is a custom data source configured use it to initialize
			if (instance.dataSource != null){
				instance.connection = instance.dataSource.getConnection();	
				
				if (ServletUtil.isAppPropertyTrue("database.reinitializeOnStart")){
					instance.initDB();
				}
				return instance.connection;
			}
			
			// otherwise initialize connection to the built-in Derby database
			try {
				//attempt to connect to the database
				instance.connection = DriverManager.getConnection(PROTOCOL+"bigbucks");

				if (ServletUtil.isAppPropertyTrue("database.reinitializeOnStart")){
					instance.initDB();
				}
			} catch (SQLException e){
				//if database does not exist, create it an initialize it
				if (e.getErrorCode() == 40000){
					instance.connection = DriverManager.getConnection(PROTOCOL+"bigbucks;create=true");
					instance.initDB();
				//otherwise pass along the exception
				} else {
					throw e;
				}
			}

		}
		
		return instance.connection;	
	}
	
	/*
	 * Create and initialize the database
	 */
	private void initDB() throws SQLException{

		Statement statement = connection.createStatement();
		
		try {
			statement.execute("DROP TABLE PEOPLE");
			statement.execute("DROP TABLE ACCOUNTS");
			statement.execute("DROP TABLE TRANSACTIONS");
			statement.execute("DROP TABLE FEEDBACK");
			statement.execute("DROP TABLE STOCKS");
			statement.execute("DROP TABLE PORTOFOLIOS");
			statement.execute("DROP TABLE INDEXES");
			statement.execute("DROP TABLE ASSET_TRACKER");
		} catch (SQLException e) {
			// not a problem
		}
		
		statement.execute("CREATE TABLE PEOPLE (USER_ID VARCHAR(50) NOT NULL, PASSWORD VARCHAR(20) NOT NULL, FIRST_NAME VARCHAR(100) NOT NULL, LAST_NAME VARCHAR(100) NOT NULL, ROLE VARCHAR(50) NOT NULL, PRIMARY KEY (USER_ID))");
		statement.execute("CREATE TABLE FEEDBACK (FEEDBACK_ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), NAME VARCHAR(100) NOT NULL, EMAIL VARCHAR(50) NOT NULL, SUBJECT VARCHAR(100) NOT NULL, COMMENTS VARCHAR(500) NOT NULL, PRIMARY KEY (FEEDBACK_ID))");
		statement.execute("CREATE TABLE ACCOUNTS (ACCOUNT_ID BIGINT NOT NULL GENERATED BY DEFAULT AS IDENTITY (START WITH 800000, INCREMENT BY 1), USERID VARCHAR(50) NOT NULL, ACCOUNT_NAME VARCHAR(100) NOT NULL, BALANCE DOUBLE NOT NULL, PRIMARY KEY (ACCOUNT_ID))");
		statement.execute("CREATE TABLE TRANSACTIONS (TRANSACTION_ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), ACCOUNTID BIGINT NOT NULL, DATE TIMESTAMP NOT NULL, STOCKID VARCHAR(50) NOT NULL, TYPE VARCHAR(100) NOT NULL, SHARE INTEGER NOT NULL, PRICE DOUBLE NOT NULL, AMOUNT DOUBLE NOT NULL, PRIMARY KEY (TRANSACTION_ID))");
		statement.execute("CREATE TABLE STOCKS (STOCK_ID VARCHAR(50) NOT NULL, STOCK_NAME VARCHAR(100) NOT NULL, PRIMARY KEY (STOCK_ID))");
		statement.execute("CREATE TABLE PORTFOLIOS (PORTFOLIO_ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), ACCOUNTID BIGINT NOT NULL, STOCKID VARCHAR(50) NOT NULL, SHARE INTEGER NOT NULL, BUYIN_PRICE DOUBLE NOT NULL, PRIMARY KEY (ACCOUNTID, STOCKID))");
		statement.execute("CREATE TABLE HISTORY_DATA (DATA_ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), STOCKID VARCHAR(50) NOT NULL, DATETIME DATE NOT NULL, PRICE_OPEN DOUBLE, PRICE_HIGH DOUBLE, PRICE_LOW DOUBLE, PRICE_CLOSE DOUBLE, PRICE_ADJ_CLOSE DOUBLE, VOLUME BIGINT, SIMPLE_RETURN DOUBLE, PRIMARY KEY (STOCKID, DATETIME))");
		statement.execute("CREATE TABLE INDEXES(INDEX_ID VARCHAR(50) NOT NULL, INDEX_NAME VARCHAR(100) NOT NULL, PRIMARY KEY (INDEX_ID))");
		statement.execute("CREATE TABLE ASSET_TRACKER(TRACKER_ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), ACCOUNTID BIGINT NOT NULL, DATETIME DATE NOT NULL, STOCK_VALUE DOUBLE NOT NULL, CASH DOUBLE NOT NULL, PRINCIPLE DOUBLE NOT NULL, PRIMARY KEY (ACCOUNTID, DATETIME))");

		statement.execute("INSERT INTO STOCKS (STOCK_ID, STOCK_NAME) VALUES ('FB', 'Meta Platforms, Inc.')");
		statement.execute("INSERT INTO STOCKS (STOCK_ID, STOCK_NAME) VALUES ('NFLX', 'Netflix, Inc.')");
		statement.execute("INSERT INTO STOCKS (STOCK_ID, STOCK_NAME) VALUES ('PEP', 'PepsiCo, Inc.')");
		statement.execute("INSERT INTO STOCKS (STOCK_ID, STOCK_NAME) VALUES ('TSLA', 'Tesla, Inc.')");
		statement.execute("INSERT INTO STOCKS (STOCK_ID, STOCK_NAME) VALUES ('TSM', 'Taiwan Semiconductor Manufacturing Company Limited')");
		statement.execute("INSERT INTO STOCKS (STOCK_ID, STOCK_NAME) VALUES ('BABA', 'Alibaba Group Holding Limited')");
		statement.execute("INSERT INTO STOCKS (STOCK_ID, STOCK_NAME) VALUES ('GOOG', 'Alphabet Inc.')");

		// Create 'admin' account to control all the users.
		statement.execute("INSERT INTO PEOPLE (USER_ID,PASSWORD,FIRST_NAME,LAST_NAME,ROLE) VALUES ('admin', 'admin', 'Admin', 'User','admin')");
		statement.execute("INSERT INTO ACCOUNTS (USERID,ACCOUNT_NAME,BALANCE) VALUES ('admin','admin', 52394783.61)");


		statement.execute("INSERT INTO PEOPLE (USER_ID,PASSWORD,FIRST_NAME,LAST_NAME,ROLE) VALUES ('dchu', 'cdc19980828%', 'Dongchen', 'Chu','user')");
		statement.execute("INSERT INTO ACCOUNTS (USERID,ACCOUNT_NAME,BALANCE) VALUES ('dchu','Stock', 1000000)");
		DBUtil.setTradeStocks("dchu","2022-02-03 15:37:32", 800001, "FB", "buy", 300, 235.9);
		DBUtil.setTradeStocks("dchu","2022-02-07 13:55:46", 800001, "FB", "buy", 100, 226.14);
		DBUtil.setTradeStocks("dchu","2022-02-08 14:31:32", 800001, "FB", "buy", 200, 217.83);
		DBUtil.setTradeStocks("dchu","2022-02-15 11:59:42", 800001, "FB", "buy", 200, 217.37);
		DBUtil.setTradeStocks("dchu","2022-02-03 14:52:47", 800001, "NFLX", "buy", 100, 406.35);
		DBUtil.setTradeStocks("dchu","2022-02-04 09:34:41", 800001, "PEP", "sell", 100, 174.5);
		DBUtil.setTradeStocks("dchu","2022-03-09 15:27:43", 800001, "PEP", "buy", 100, 158.13);
		DBUtil.setTradeStocks("dchu","2022-02-07 15:00:23", 800001, "TSLA", "buy", 100, 919);
		DBUtil.setTradeStocks("dchu","2022-03-09 15:28:14", 800001, "TSLA", "buy", 200, 858.54);
		DBUtil.setTradeStocks("dchu","2022-02-01 14:05:42", 800001, "TSM", "buy", 100, 122.03);
		DBUtil.setTradeStocks("dchu","2022-02-03 14:55:01", 800001, "TSM", "buy", 300, 120.3);
		DBUtil.setTradeStocks("dchu","2022-04-06 13:55:46", 800001, "TSM", "buy", 100, 100.65);

		statement.execute("INSERT INTO PEOPLE (USER_ID,PASSWORD,FIRST_NAME,LAST_NAME,ROLE) VALUES ('jlenz', 'demo123', 'Jimmie', 'Lenz','user')");
		statement.execute("INSERT INTO ACCOUNTS (USERID,ACCOUNT_NAME,BALANCE) VALUES ('jlenz','Stock', 1000000)");
		DBUtil.setTradeStocks("jlenz","2022-02-11 15:30:00", 800002, "BABA", "buy", 300, 122.2);
		DBUtil.setTradeStocks("jlenz","2022-02-14 10:15:46", 800002, "BABA", "buy", 100, 120);
		DBUtil.setTradeStocks("jlenz","2022-03-09 11:15:32", 800002, "BABA", "buy", 200, 99.73);
		DBUtil.setTradeStocks("jlenz","2022-03-15 13:59:42", 800002, "BABA", "buy", 200, 74.85);
		DBUtil.setTradeStocks("jlenz","2022-02-23 09:52:47", 800002, "GOOG", "buy", 100, 2608.54);
		DBUtil.setTradeStocks("jlenz","2022-03-07 14:34:41", 800002, "GOOG", "buy", 100, 3545.84);
		DBUtil.setTradeStocks("jlenz","2022-03-29 10:27:43", 800002, "GOOG", "sell", 100, 2854.16);

		// Index Database:
		statement.execute("INSERT INTO INDEXES (INDEX_ID,INDEX_NAME) VALUES ('^DJI','Dow Jones Industrial Average')");
		statement.execute("INSERT INTO INDEXES (INDEX_ID,INDEX_NAME) VALUES ('^GSPC','S&P 500')");
		statement.execute("INSERT INTO INDEXES (INDEX_ID,INDEX_NAME) VALUES ('^IXIC','NASDAQ Composite')");

		Log4BigBucks.getInstance().logInfo("Database initialized");
	}

	/**
	 * Create account
	 * @param fn
	 * @param ln
	 * @param un
	 * @param passw
	 * @return error message
	 */

	public static String createAccount (String fn, String ln, String un, String passw) {

		String error1 = addUser(un, passw, fn, ln);
		if (error1 != null) {
			return error1;
		}

		String error2 = addAccount(un, "Stock");
		if (error2 != null) {
			return error2;
		}

		Log4BigBucks.getInstance().logInfo("Create Account for: " + un);
		return null;
	}


	/**
	 * Retrieve feedback details
	 * @param feedbackId specific feedback ID to retrieve or Feedback.FEEDBACK_ALL to retrieve all stored feedback submissions
	 */
	public static ArrayList<Feedback> getFeedback (long feedbackId){
		ArrayList<Feedback> feedbackList = new ArrayList<Feedback>();
		
		try { 
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			
			String query = "SELECT * FROM FEEDBACK";
			
			if (feedbackId != Feedback.FEEDBACK_ALL){
				query = query + " WHERE FEEDBACK_ID = "+ feedbackId +"";
			}
			
			ResultSet resultSet = statement.executeQuery(query);
	
			while (resultSet.next()){
				String name = resultSet.getString("NAME");
				String email = resultSet.getString("EMAIL");
				String subject = resultSet.getString("SUBJECT");
				String message = resultSet.getString("COMMENTS");
				long id = resultSet.getLong("FEEDBACK_ID");
				Feedback feedback = new Feedback(id, name, email, subject, message);
				feedbackList.add(feedback);
			}
		} catch (SQLException e) {
			Log4BigBucks.getInstance().logError("Error retrieving feedback: " + e.getMessage());
		}
		
		return feedbackList;
	}
	
	
	/**
	 * Authenticate user
	 * @param user user name
	 * @param password password
	 * @return true if valid user, false otherwise
	 * @throws SQLException
	 */
	public static boolean isValidUser(String user, String password) throws SQLException{
		if (user == null || password == null || user.trim().length() == 0 || password.trim().length() == 0)
			return false; 
		
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		
		ResultSet resultSet =statement.executeQuery("SELECT COUNT(*)FROM PEOPLE WHERE USER_ID = '"+ user +"' AND PASSWORD='" + password + "'"); /* BAD - user input should always be sanitized */
		
		if (resultSet.next()){
			
				if (resultSet.getInt(1) > 0)
					return true;
		}
		return false;
	}
	

	/**
	 * Get user information
	 * @param username
	 * @return user information
	 * @throws SQLException
	 */
	public static User getUserInfo(String username) throws SQLException{
		if (username == null || username.trim().length() == 0)
			return null; 
		
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet =statement.executeQuery("SELECT FIRST_NAME,LAST_NAME,ROLE FROM PEOPLE WHERE USER_ID = '"+ username +"' "); /* BAD - user input should always be sanitized */

		String firstName = null;
		String lastName = null;
		String roleString = null;
		if (resultSet.next()){
			firstName = resultSet.getString("FIRST_NAME");
			lastName = resultSet.getString("LAST_NAME");
			roleString = resultSet.getString("ROLE");
		}
		
		if (firstName == null || lastName == null)
			return null;
		
		User user = new User(username, firstName, lastName);
		
		if (roleString.equalsIgnoreCase("admin"))
			user.setRole(Role.Admin);
		
		return user;
	}

	/**
	 * Get all accounts for the specified user
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public static Account[] getAccounts(String username) throws SQLException{
		if (username == null || username.trim().length() == 0)
			return null; 
		
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet =statement.executeQuery("SELECT ACCOUNT_ID, ACCOUNT_NAME, BALANCE FROM ACCOUNTS WHERE USERID = '"+ username +"' "); /* BAD - user input should always be sanitized */

		ArrayList<Account> accounts = new ArrayList<Account>(3);
		while (resultSet.next()){
			long accountId = resultSet.getLong("ACCOUNT_ID");
			String name = resultSet.getString("ACCOUNT_NAME");
			double balance = resultSet.getDouble("BALANCE"); 
			Account newAccount = new Account(accountId, name, balance);
			accounts.add(newAccount);
		}
		
		return accounts.toArray(new Account[accounts.size()]);
	}

	public static Stock[] getStocks() throws SQLException {

		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT STOCK_ID,STOCK_NAME FROM STOCKS ORDER BY STOCK_ID");

		int rowcount = 3;//countSet.getInt("nums");

		ArrayList<Stock> stocks = new ArrayList<Stock>((rowcount));
		while (resultSet.next()) {
			String stockId = resultSet.getString("STOCK_ID");
			String stockName = resultSet.getString("STOCK_NAME");
			//double price = resultSet.getDouble("PRICE");
			Stock newStock = new Stock(stockId, stockName);
			stocks.add(newStock);
		}

		return stocks.toArray(new Stock[stocks.size()]);
	}

	public static Index[] getIndexes() throws SQLException {

		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT INDEX_ID,INDEX_NAME FROM INDEXES ORDER BY INDEX_ID");

		int rowcount = 3;//countSet.getInt("nums");

		ArrayList<Index> indexes = new ArrayList<Index>((rowcount));
		while (resultSet.next()) {
			String indexId = resultSet.getString("INDEX_ID");
			String indexName = resultSet.getString("INDEX_NAME");
			//double price = resultSet.getDouble("PRICE");
			Index newIndex = new Index(indexId, indexName);
			indexes.add(newIndex);
		}

		return indexes.toArray(new Index[indexes.size()]);
	}

	public static ArrayList<Profile> getRiskRetHistory(long accountId) throws SQLException, IOException, ParseException, InterruptedException {
		Connection connection = DBUtil.getConnection();
		connection.setAutoCommit(false);
		String query = "SELECT DISTINCT DATETIME FROM HISTORY_DATA WHERE DATETIME >= CAST((select min(DATE) from TRANSACTIONS where ACCOUNTID = ?) AS DATE)  ORDER BY DATETIME";
		PreparedStatement ps = connection.prepareStatement(query);
		ps.setLong(1, accountId);
		ResultSet rs = ps.executeQuery();
		ArrayList<Profile> profiles = new ArrayList<>();
		while(rs.next()){
			LocalDate date = rs.getTimestamp("datetime").toLocalDateTime().toLocalDate();
			profiles.add(new Profile(date, DBUtil.init_balance,0,DBUtil.init_balance,0,0));

		}
		ps.close();

		//connection.setAutoCommit(false);
		String query2 = "SELECT DISTINCT STOCKID FROM transactions where ACCOUNTID = ?";
		PreparedStatement ps2 = connection.prepareStatement(query2);
		ps2.setLong(1,accountId);
		ResultSet rs2 = ps2.executeQuery();

//        double assetValue = 0;
		while(rs2.next()){
			String symbol = rs2.getString("STOCKID");
			APIUtil.getHistorical(symbol,"datetime");
			List<Position> aPerformance =getOrderFromTransaction(accountId,symbol);
			for (Position p: aPerformance) {
				System.out.println(p.date + "::" + p.symbol + "::" + p.share + "::" + p.marketprice + "::" + p.filledprice);
			}

			int profilesIx = 0;
			int performIx = 0;
			double cashcon = 0;
			double lastEquity = init_balance;
			while(profilesIx<profiles.size() && performIx<aPerformance.size()){

				LocalDate aDate =  profiles.get(profilesIx).date;
				LocalDate bDate = aPerformance.get(performIx).date;
				if (aDate.isEqual(bDate) ){
					if (aPerformance.get(performIx).cashchg!=0) {
						cashcon =cashcon+ aPerformance.get(performIx).cashchg;
						//System.out.println(cashcon);
					}
					double cash = profiles.get(profilesIx).cash + cashcon;
//                    assetValue = assetValue + aPerformance.get(performIx).assetchg;
					//System.out.println(aDate+"::"+symbol+"::"+cash+"::"+profiles.get(profilesIx).cash+"::"+cashcon);

					profiles.get(profilesIx).setCash(cash);
					profiles.get(profilesIx).setAsset(profiles.get(profilesIx).asset + aPerformance.get(performIx).amount);
					profiles.get(profilesIx).setEquity(profiles.get(profilesIx).cash + profiles.get(profilesIx).asset);

					double equity = profiles.get(profilesIx).equity;
					profiles.get(profilesIx).setPnL(equity - lastEquity);
					profiles.get(profilesIx).setPctPnL(profiles.get(profilesIx).PnL / lastEquity);
					lastEquity = equity;

					profilesIx++;
					performIx++;

				}else{
					profilesIx++;
				}
			}
		}
		ps2.close();
//        profiles.remove(0);
		return profiles;
	}

	public static ArrayList<Position> getOrderFromTransaction(long accountId ,String symbol) throws SQLException, IOException, ParseException, InterruptedException {
		APIUtil.getHistorical(symbol,"price_adj_close");

		Connection connection = DBUtil.getConnection();

		String query = "SELECT * FROM TRANSACTIONS a join HISTORY_DATA b on a.STOCKID = b.STOCKID and b.DATETIME = CAST(a.DATE as DATE) WHERE ACCOUNTID = ? AND a.STOCKID = ? ORDER BY a.DATE";
		PreparedStatement ps = connection.prepareStatement(query);
		ps.setLong(1, accountId);
		ps.setString(2, symbol);
		ResultSet rs = ps.executeQuery();
		ArrayList<Position> orders = new ArrayList<Position>();

		while (rs.next()){
			LocalDate date = rs.getTimestamp("DATE").toLocalDateTime().toLocalDate();

			int sharechg = rs.getInt("share");
			double filledprice = rs.getDouble("price");
			double marketprice = rs.getDouble("price_adj_close");
			String type = rs.getString("type");
			double amountchg = sharechg * filledprice;
			if (type.equals("sell")) {
				amountchg = -amountchg;
				sharechg = -sharechg;
			}
			final double cashchg = -amountchg;
			final double assetchg = amountchg;
			orders.add(new Position(date, symbol, accountId, sharechg, amountchg, filledprice,marketprice,cashchg,assetchg,0,0));

		}
		//System.out.println(orders);

		ps.close();

		ArrayList<Position> performance = new ArrayList<Position>();
		String query2 = "SELECT * FROM HISTORY_DATA WHERE STOCKID = ? AND DATETIME >= ( CAST((select min(DATE) from TRANSACTIONS where ACCOUNTID = ? AND STOCKID = ?) AS DATE)) ORDER BY DATETIME";
		PreparedStatement ps2 = connection.prepareStatement(query2);
		ps2.setString(1,symbol);
		ps2.setLong(2,accountId);
		ps2.setString(3,symbol);
		ResultSet rs2 = ps2.executeQuery();
		int orderIndex = 0;
		int share = 0;
		double filledprice=0;
		double amount=0;
		while(rs2.next()){
			// get data from historical data
			double mktprice = rs2.getDouble("price_adj_close");
			LocalDate date = rs2.getTimestamp("DATETIME").toLocalDateTime().toLocalDate();

			//get data from transation
			//System.out.println(date.isEqual(orders.get(orderIndex).date));
			if(date.isEqual(orders.get(orderIndex).date)){
				double assetchg = orders.get(orderIndex).assetchg;
				double cashchg = orders.get(orderIndex).cashchg;
				int sharechg = orders.get(orderIndex).sharechg;
				//System.out.println(date+"::"+sharechg+"::"+symbol+"::"+assetchg+"::"+cashchg);
//                filledprice = orders.get(orderIndex).filledprice;
				share+=sharechg;

				amount = share * mktprice;
//                amount = amount+assetchg;

				filledprice = assetchg/sharechg;
				double PnL = (mktprice-filledprice)*sharechg;
				double pctPnL = PnL/amount;
				// we do have transaction on that day
				performance.add(new Position(date, symbol, accountId, sharechg, amount, filledprice,mktprice,cashchg,assetchg,PnL,share));
				//System.out.println(new Position(date, symbol, accountId, sharechg, amount, filledprice,mktprice,cashchg,assetchg,PnL,share));

				if(orderIndex<orders.size()-1) {
					orderIndex = orderIndex+1;
				}
			}else{
				double PnL = (mktprice-filledprice)*share;
				double pctPnL = PnL/amount;
				amount = share * mktprice;
				performance.add(new Position(date, symbol, accountId, share, amount, filledprice,mktprice,0,0,PnL,share));
				//System.out.println(new Position(date, symbol, accountId, share, amount, filledprice,mktprice,0,0,PnL,share));
			}
		}

		return performance;
	}

	public static ArrayList<Profile> getAllUsersRiskRetHistory() throws SQLException, IOException, ParseException, InterruptedException {
		Connection connection = DBUtil.getConnection();
		connection.setAutoCommit(false);
		String query0 = "SELECT COUNT(ACCOUNT_ID) AS USER_COUNT FROM ACCOUNTS";
		PreparedStatement ps0 = connection.prepareStatement(query0);
		ResultSet rs0 = ps0.executeQuery();
		double all_balance = init_balance;
		if (rs0.next()) {
			all_balance *= rs0.getInt("USER_COUNT");
		}
		ps0.close();

		String query = "SELECT DISTINCT DATETIME FROM HISTORY_DATA WHERE DATETIME >= CAST((select min(DATE) from TRANSACTIONS) AS DATE)  ORDER BY DATETIME";
		PreparedStatement ps = connection.prepareStatement(query);
		//ps.setLong(1, accountId);
		ResultSet rs = ps.executeQuery();
		ArrayList<Profile> profiles = new ArrayList<>();
		while(rs.next()){
			LocalDate date = rs.getTimestamp("datetime").toLocalDateTime().toLocalDate();
			profiles.add(new Profile(date, all_balance,0,all_balance,0,0));

		}
		ps.close();
		String query2 = "SELECT DISTINCT STOCKID FROM transactions";
		PreparedStatement ps2 = connection.prepareStatement(query2);
		//ps2.setLong(1,accountId);
		ResultSet rs2 = ps2.executeQuery();

//        double assetValue = 0;
		while(rs2.next()){
			String symbol = rs2.getString("STOCKID");
			APIUtil.getHistorical(symbol,"datetime");
			List<Position> aPerformance =getAllUsersOrderFromTransaction(symbol);

			int profilesIx = 0;
			int performIx = 0;
			double cashcon = 0;
			double lastEquity = all_balance;
			while(profilesIx<profiles.size() && performIx<aPerformance.size()){

				LocalDate aDate =  profiles.get(profilesIx).date;
				LocalDate bDate = aPerformance.get(performIx).date;
				if (aDate.isEqual(bDate) ){
					if (aPerformance.get(performIx).cashchg!=0) {
						cashcon =cashcon+ aPerformance.get(performIx).cashchg;
						//System.out.println(cashcon);
					}
					double cash = profiles.get(profilesIx).cash + cashcon;
//                    assetValue = assetValue + aPerformance.get(performIx).assetchg;
					//System.out.println(aDate+"::"+symbol+"::"+cash+"::"+profiles.get(profilesIx).cash+"::"+cashcon);

					profiles.get(profilesIx).setCash(cash);
					profiles.get(profilesIx).setAsset(profiles.get(profilesIx).asset + aPerformance.get(performIx).amount);
					profiles.get(profilesIx).setEquity(profiles.get(profilesIx).cash + profiles.get(profilesIx).asset);

					double equity = profiles.get(profilesIx).equity;
					profiles.get(profilesIx).setPnL(equity - lastEquity);
					profiles.get(profilesIx).setPctPnL(profiles.get(profilesIx).PnL / lastEquity);
					lastEquity = equity;

					profilesIx++;
					performIx++;

				}else{
					profilesIx++;
				}
			}
		}
//        profiles.remove(0);
		return profiles;
	}

	public static ArrayList<Position> getAllUsersOrderFromTransaction(String symbol) throws SQLException, IOException, ParseException, InterruptedException {
		APIUtil.getHistorical(symbol,"price_adj_close");

		Connection connection = DBUtil.getConnection();

		String query = "SELECT * FROM TRANSACTIONS a join HISTORY_DATA b on a.STOCKID = b.STOCKID and b.DATETIME = CAST(a.DATE as DATE) WHERE  a.STOCKID = ? ORDER BY a.DATE";
		PreparedStatement ps = connection.prepareStatement(query);
		//ps.setLong(1, accountId);
		ps.setString(1, symbol);
		ResultSet rs = ps.executeQuery();
		ArrayList<Position> orders = new ArrayList<Position>();

		while (rs.next()){
			LocalDate date = rs.getTimestamp("DATE").toLocalDateTime().toLocalDate();

			int sharechg = rs.getInt("share");
			double filledprice = rs.getDouble("price");
			double marketprice = rs.getDouble("price_adj_close");
			String type = rs.getString("type");
			double amountchg = sharechg * filledprice;
			if (type.equals("sell")) {
				amountchg = -amountchg;
				sharechg = -sharechg;
			}
			final double cashchg = -amountchg;
			final double assetchg = amountchg;
			orders.add(new Position(date, symbol, -1, sharechg, amountchg, filledprice,marketprice,cashchg,assetchg,0,0));

		}
		//System.out.println(orders);

		ps.close();

		ArrayList<Position> performance = new ArrayList<Position>();
		String query2 = "SELECT * FROM HISTORY_DATA WHERE STOCKID = ? AND DATETIME >= ( CAST((select min(DATE) from TRANSACTIONS where STOCKID = ?) AS DATE)) ORDER BY DATETIME";
		PreparedStatement ps2 = connection.prepareStatement(query2);
		ps2.setString(1,symbol);
		//ps2.setLong(2,accountId);
		ps2.setString(2,symbol);
		ResultSet rs2 = ps2.executeQuery();
		int orderIndex = 0;
		int share = 0;
		double filledprice=0;
		double amount=0;
		while(rs2.next()){
			// get data from historical data
			double mktprice = rs2.getDouble("price_adj_close");
			LocalDate date = rs2.getTimestamp("DATETIME").toLocalDateTime().toLocalDate();

			//get data from transation
			//System.out.println(date.isEqual(orders.get(orderIndex).date));
			if(date.isEqual(orders.get(orderIndex).date)){
				double assetchg = orders.get(orderIndex).assetchg;
				double cashchg = orders.get(orderIndex).cashchg;
				int sharechg = orders.get(orderIndex).sharechg;
				//System.out.println(date+"::"+sharechg+"::"+symbol+"::"+assetchg+"::"+cashchg);
//                filledprice = orders.get(orderIndex).filledprice;
				share+=sharechg;

				amount = share * mktprice;
//                amount = amount+assetchg;

				filledprice = assetchg/sharechg;
				double PnL = (mktprice-filledprice)*sharechg;
				double pctPnL = PnL/amount;
				// we do have transaction on that day
				performance.add(new Position(date, symbol, -1, sharechg, amount, filledprice,mktprice,cashchg,assetchg,PnL,share));
				//System.out.println(new Position(date, symbol, accountId, sharechg, amount, filledprice,mktprice,cashchg,assetchg,PnL,share));

				if(orderIndex<orders.size()-1) {
					orderIndex = orderIndex+1;
				}
			}else{
				double PnL = (mktprice-filledprice)*share;
				double pctPnL = PnL/amount;
				amount = share * mktprice;
				performance.add(new Position(date, symbol, -1, share, amount, filledprice,mktprice,0,0,PnL,share));
				//System.out.println(new Position(date, symbol, accountId, share, amount, filledprice,mktprice,0,0,PnL,share));
			}
		}

		return performance;
	}

	/**
	 * Transfer funds between specified accounts
	 * @param username
	 * @param creditActId
	 * @param debitActId
	 * @param amount
	 * @return
	 */
	public static String transferFunds(String username, long creditActId, long debitActId, double amount) {
				
		try {
			
			User user = getUserInfo(username);
			
			Connection connection = getConnection();
			Statement statement = connection.createStatement();

			Account debitAccount = Account.getAccount(debitActId);
			Account creditAccount = Account.getAccount(creditActId);

			if (debitAccount == null){
				return "Originating account is invalid";
			} 
			
			if (creditAccount == null)
				return "Destination account is invalid";
			
			java.sql.Timestamp date = new Timestamp(new java.util.Date().getTime());
			
			//in real life we would want to do these updates and transaction entry creation
			//as one atomic operation
			
			long userCC = user.getCreditCardNumber();
			
			/* this is the account that the payment will be made from, thus negative amount!*/
			double debitAmount = -amount; 
			/* this is the account that the payment will be made to, thus positive amount!*/
			double creditAmount = amount;
			
			/* Credit card account balance is the amount owed, not amount owned 
			 * (reverse of other accounts). Therefore we have to process balances differently*/
			if (debitAccount.getAccountId() == userCC)
				debitAmount = -debitAmount;
		
			//create transaction record
			statement.execute("INSERT INTO TRANSACTIONS (ACCOUNTID, DATE, TYPE, AMOUNT) VALUES ("+debitAccount.getAccountId()+",'"+date+"',"+((debitAccount.getAccountId() == userCC)?"'Cash Advance'":"'Withdrawal'")+","+debitAmount+")," +
					  "("+creditAccount.getAccountId()+",'"+date+"',"+((creditAccount.getAccountId() == userCC)?"'Payment'":"'Deposit'")+","+creditAmount+")"); 	

			Log4BigBucks.getInstance().logTransaction(debitAccount.getAccountId()+" - "+ debitAccount.getAccountName(), creditAccount.getAccountId()+" - "+ creditAccount.getAccountName(), amount);
			
			if (creditAccount.getAccountId() == userCC)
				 creditAmount = -creditAmount;
			
			//add cash advance fee since the money transfer was made from the credit card 
			if (debitAccount.getAccountId() == userCC){
				statement.execute("INSERT INTO TRANSACTIONS (ACCOUNTID, DATE, TYPE, AMOUNT) VALUES ("+debitAccount.getAccountId()+",'"+date+"','Cash Advance Fee',"+CASH_ADVANCE_FEE+")");
				debitAmount += CASH_ADVANCE_FEE;
				Log4BigBucks.getInstance().logTransaction(String.valueOf(userCC), "N/A", CASH_ADVANCE_FEE);
			}
						
			//update account balances
			statement.execute("UPDATE ACCOUNTS SET BALANCE = " + (debitAccount.getBalance()+debitAmount) + " WHERE ACCOUNT_ID = " + debitAccount.getAccountId());
			statement.execute("UPDATE ACCOUNTS SET BALANCE = " + (creditAccount.getBalance()+creditAmount) + " WHERE ACCOUNT_ID = " + creditAccount.getAccountId());
			
			return null;
			
		} catch (SQLException e) {
			return "Transaction failed. Please try again later.";
		}
	}

	public static String tradeStocks(String username, long debitActId, String StockId, String tradeType, int share, double price, double amount) {
		try {

			User user = getUserInfo(username);

			Connection connection = getConnection();
			Statement statement = connection.createStatement();

			Account debitAccount = Account.getAccount(debitActId);


			if (debitAccount == null){
				return "Trading account is invalid";
			}

			java.sql.Timestamp date = new Timestamp(new java.util.Date().getTime());

			//in real life we would want to do these updates and transaction entry creation
			//as one atomic operation

			/* this is the account that the payment will be made from, thus negative amount!*/
			double debitAmount = amount;
			if (tradeType.equals("buy")) {
				debitAmount = -debitAmount;
			}
			int debitShare = share;
			if (tradeType.equals("sell")) {
				debitShare = -debitShare;
			}

			//create transaction record
			statement.execute("INSERT INTO TRANSACTIONS (ACCOUNTID, DATE, STOCKID, TYPE, SHARE, PRICE, AMOUNT) VALUES ("+debitAccount.getAccountId()+",'"+date+"','"+StockId+"','"+tradeType+"',"+share+","+price+","+amount+")");

			String query = "SELECT SHARE, BUYIN_PRICE FROM PORTFOLIOS WHERE (ACCOUNTID=" + debitAccount.getAccountId() + ") " + "AND (STOCKID='" + StockId + "') ";
			ResultSet resultSet = statement.executeQuery(query);

			if (resultSet.next()) {
				int preShare = resultSet.getInt("SHARE");
				double prePrice = resultSet.getDouble("BUYIN_PRICE");
				double finalPrice = (prePrice * preShare + amount) / (preShare + share);
				statement.execute("UPDATE PORTFOLIOS SET SHARE = " + (preShare + debitShare) + ", BUYIN_PRICE = " + finalPrice + " WHERE (ACCOUNTID = " + debitAccount.getAccountId() + ") AND ( STOCKID = '" + StockId +"')");
			}
			else {
				statement.execute("INSERT INTO PORTFOLIOS (ACCOUNTID, STOCKID, SHARE, BUYIN_PRICE) VALUES (" + debitAccount.getAccountId()+",'"+StockId+"',"+share+","+price+")");
			}

			Log4BigBucks.getInstance().logTrade(debitAccount.getAccountId()+" - "+ debitAccount.getAccountName(), tradeType,StockId + " - " + Stock.getStock(StockId).getStockName(), share, price, amount);

			//update account balances

			statement.execute("UPDATE ACCOUNTS SET BALANCE = " + (debitAccount.getBalance()+debitAmount) + " WHERE ACCOUNT_ID = " + debitAccount.getAccountId());

			return null;

		} catch (SQLException e) {
			//return "Transaction failed. Please try again later.";
			return e.getMessage();
		}
	}

	public static String setTradeStocks(String username, String datetime, long debitActId, String StockId, String tradeType, int share, double price) {
		try {
			double amount = share*price;

			User user = getUserInfo(username);

			Connection connection = getConnection();
			Statement statement = connection.createStatement();

			Account debitAccount = Account.getAccount(debitActId);


			if (debitAccount == null){
				return "Trading account is invalid";
			}

			//java.sql.Timestamp date = new Timestamp(new java.util.Date().getTime());
			long time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datetime, new ParsePosition(0)).getTime() / 1000;
			java.sql.Timestamp date = new Timestamp(time*1000L);

			//in real life we would want to do these updates and transaction entry creation
			//as one atomic operation

			/* this is the account that the payment will be made from, thus negative amount!*/
			double debitAmount = amount;
			if (tradeType.equals("buy")) {
				debitAmount = -debitAmount;
			}
			int debitShare = share;
			if (tradeType.equals("sell")) {
				debitShare = -debitShare;
			}

			//create transaction record
			statement.execute("INSERT INTO TRANSACTIONS (ACCOUNTID, DATE, STOCKID, TYPE, SHARE, PRICE, AMOUNT) VALUES ("+debitAccount.getAccountId()+",'"+date+"','"+StockId+"','"+tradeType+"',"+share+","+price+","+amount+")");

			String query = "SELECT SHARE, BUYIN_PRICE FROM PORTFOLIOS WHERE (ACCOUNTID=" + debitAccount.getAccountId() + ") " + "AND (STOCKID='" + StockId + "') ";
			ResultSet resultSet = statement.executeQuery(query);

			if (resultSet.next()) {
				int preShare = resultSet.getInt("SHARE");
				double prePrice = resultSet.getDouble("BUYIN_PRICE");
				double finalPrice = (prePrice * preShare + amount) / (preShare + share);
				statement.execute("UPDATE PORTFOLIOS SET SHARE = " + (preShare + debitShare) + ", BUYIN_PRICE = " + finalPrice + " WHERE (ACCOUNTID = " + debitAccount.getAccountId() + ") AND ( STOCKID = '" + StockId +"')");
			}
			else {
				statement.execute("INSERT INTO PORTFOLIOS (ACCOUNTID, STOCKID, SHARE, BUYIN_PRICE) VALUES (" + debitAccount.getAccountId()+",'"+StockId+"',"+share+","+price+")");
			}

			Log4BigBucks.getInstance().logTrade(debitAccount.getAccountId()+" - "+ debitAccount.getAccountName(), tradeType,StockId + " - " + Stock.getStock(StockId).getStockName(), share, price, amount);

			//update account balances

			statement.execute("UPDATE ACCOUNTS SET BALANCE = " + (debitAccount.getBalance()+debitAmount) + " WHERE ACCOUNT_ID = " + debitAccount.getAccountId());

			return null;

		} catch (SQLException e) {
			//return "Transaction failed. Please try again later.";
			return e.getMessage();
		}
	}


	/**
	 * Get transaction information for the specified accounts in the date range (non-inclusive of the dates)
	 * @param startDate
	 * @param endDate
	 * @param accounts
	 * @param rowCount
	 * @return
	 */
	public static Transaction[] getTransactions(String startDate, String endDate, Account[] accounts, int rowCount) throws SQLException {
		
		if (accounts == null || accounts.length == 0)
			return null;

			Connection connection = getConnection();

			
			Statement statement = connection.createStatement();
			
			if (rowCount > 0)
				statement.setMaxRows(rowCount);

			StringBuffer acctIds = new StringBuffer();
			acctIds.append("ACCOUNTID = " + accounts[0].getAccountId());
			for (int i=1; i<accounts.length; i++){
				acctIds.append(" OR ACCOUNTID = "+accounts[i].getAccountId());	
			}
			
			String dateString = null;
			
			if (startDate != null && startDate.length()>0 && endDate != null && endDate.length()>0){
				dateString = "DATE BETWEEN '" + startDate + " 00:00:00' AND '" + endDate + " 23:59:59'";
			} else if (startDate != null && startDate.length()>0){
				dateString = "DATE > '" + startDate +" 00:00:00'";
			} else if (endDate != null && endDate.length()>0){
				dateString = "DATE < '" + endDate + " 23:59:59'";
			}
			
			String query = "SELECT * FROM TRANSACTIONS WHERE (" + acctIds.toString() + ") " + ((dateString==null)?"": "AND (" + dateString + ") ") + "ORDER BY DATE DESC" ;
			ResultSet resultSet = null;
			
			try {
				resultSet = statement.executeQuery(query);
			} catch (SQLException e){
				int errorCode = e.getErrorCode();
				if (errorCode == 30000)
					throw new SQLException("Date-time query must be in the format of yyyy-mm-dd HH:mm:ss", e);
				
				throw e;
			}
			ArrayList<Transaction> transactions = new ArrayList<Transaction>();
			while (resultSet.next()){
				int transId = resultSet.getInt("TRANSACTION_ID");
				long actId = resultSet.getLong("ACCOUNTID");
				Timestamp date = resultSet.getTimestamp("DATE");
				String stockId = resultSet.getString("STOCKID");
				String desc = resultSet.getString("TYPE");
				int share = resultSet.getInt("SHARE");
				double price = resultSet.getDouble("PRICE");
				double amount = resultSet.getDouble("AMOUNT");
				transactions.add(new Transaction(transId, actId, date, stockId, desc, share, price, amount));
			}
			
			return transactions.toArray(new Transaction[transactions.size()]); 
	}

	private static ArrayList<Stock> get1dayTransactionStock(String date) throws SQLException {
		long startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date + " 00:00:00", new ParsePosition(0)).getTime() / 1000;
		long endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date + " 23:59:59", new ParsePosition(0)).getTime() / 1000;
		java.sql.Timestamp start = new Timestamp(startTime*1000L);
		java.sql.Timestamp end = new Timestamp(endTime*1000L);

		Connection connection = getConnection();
		String query = "SELECT DISTINCT STOCKID FROM TRANSACTIONS WHERE DATE BETWEEN ? and ? ORDER BY STOCKID";
		PreparedStatement ps = connection.prepareStatement(query);
		ps.setTimestamp(1,start);
		ps.setTimestamp(2,end);
		ResultSet rs = ps.executeQuery();
		ArrayList<Stock> stocks = new ArrayList<>();
		while (rs.next()) {
			stocks.add(Stock.getStock(rs.getString("STOCKID")));
		}

		ps.close();
		return stocks;
	}

	public static ArrayList<TransactionSummary> get1dayAllTransactions(String date) throws SQLException {
		ArrayList<Stock> stocks = get1dayTransactionStock(date);
		ArrayList<TransactionSummary> result = new ArrayList<>();
		for (Stock stock: stocks) {
			result.add(get1dayAllTransaction(date,stock.getStockId()));
		}

		return result;
	}

	private static TransactionSummary get1dayAllTransaction(String date, String symbol) throws SQLException {
		long startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date + " 00:00:00", new ParsePosition(0)).getTime() / 1000;
		long endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date + " 23:59:59", new ParsePosition(0)).getTime() / 1000;
		java.sql.Timestamp start = new Timestamp(startTime*1000L);
		java.sql.Timestamp end = new Timestamp(endTime*1000L);

		Connection connection = getConnection();
		String query = "SELECT TYPE,SHARE FROM TRANSACTIONS WHERE (DATE BETWEEN ? and ?) AND STOCKID=?  ORDER BY STOCKID";
		PreparedStatement ps = connection.prepareStatement(query);
		ps.setTimestamp(1,start);
		ps.setTimestamp(2,end);
		ps.setString(3,symbol);
		ResultSet rs = ps.executeQuery();
		TransactionSummary ts = new TransactionSummary(symbol, 0, 0);
		while (rs.next()) {
			String type = rs.getString("TYPE");
			int share = rs.getInt("SHARE");
			if (type.equals("buy")) {
				ts.setBuyShare(ts.getBuyShare()+share);
			}
			else {
				ts.setSellShare(ts.getSellShare()+share);
			}
		}
		ps.close();

		return ts;
	}

	public static Portfolio[] getPortfolios(Account[] accounts) throws SQLException {
		if (accounts == null || accounts.length == 0)
			return null;

		Connection connection = getConnection();

		Statement statement = connection.createStatement();

		StringBuffer acctIds = new StringBuffer();
		acctIds.append("ACCOUNTID = " + accounts[0].getAccountId());
		for (int i=1; i<accounts.length; i++) {
			acctIds.append(" OR ACCOUNTID = "+accounts[i].getAccountId());
		}

		String query = "SELECT * FROM PORTFOLIOS WHERE (" + acctIds.toString() + ")" + "ORDER BY STOCKID";
		ResultSet resultSet = null;

		try {
			resultSet = statement.executeQuery(query);
		} catch (SQLException e) {
			throw e;
		}
		ArrayList<Portfolio> portfolios = new ArrayList<Portfolio>();
		while (resultSet.next()) {
			int portId = resultSet.getInt("PORTFOLIO_ID");
			long accountId = resultSet.getLong("ACCOUNTID");
			String stockId = resultSet.getString("STOCKID");
			int share = resultSet.getInt("SHARE");
			double buyinPrice = resultSet.getDouble("BUYIN_PRICE");
			portfolios.add(new Portfolio(portId, accountId, stockId, share, buyinPrice));
		}

		return portfolios.toArray(new Portfolio[portfolios.size()]);
	}

	public static ArrayList<Stock> getAllUsersStock() throws SQLException {
		Connection connection = getConnection();
		String query = "SELECT DISTINCT STOCKID FROM PORTFOLIOS ORDER BY STOCKID";
		PreparedStatement ps = connection.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		ArrayList<Stock> stocks = new ArrayList<>();
		while(rs.next()) {
			String stockId = rs.getString("STOCKID");
			stocks.add(Stock.getStock(stockId));
		}
		ps.close();

		return stocks;
	}

	public static ArrayList<Portfolio> getAllUsersPortfolios() throws SQLException {
		ArrayList<Stock> stocks = getAllUsersStock();
		ArrayList<Portfolio> portfolios = new ArrayList<>();
		for (Stock stock: stocks) {
			portfolios.add(getAllUsersPortfolio(stock.getStockId()));
		}
		return portfolios;
	}

	private static Portfolio getAllUsersPortfolio(String symbol) throws SQLException {
		Connection connection = getConnection();
		String query = "SELECT * FROM PORTFOLIOS WHERE STOCKID = ?";
		PreparedStatement ps = connection.prepareStatement(query);
		ps.setString(1,symbol);
		ResultSet rs = ps.executeQuery();
		Portfolio result = new Portfolio(-1, -1, symbol, 0, 0);
		while(rs.next()) {
			int share = rs.getInt("SHARE");
			double price = rs.getDouble("BUYIN_PRICE");
			int newshare = result.getShare() + share;
			double newprice = (result.getBuyinPrice() * result.getShare() + share * price)/(result.getShare() + share);
			result.setShare(newshare);
			result.setBuyinPrice(newprice);
		}
		ps.close();
		return result;
	}

	public static String[] getBankUsernames() {
		
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			//at the moment this query limits transfers to
			//transfers between two user accounts
			ResultSet resultSet =statement.executeQuery("SELECT USER_ID FROM PEOPLE"); 

			ArrayList<String> users = new ArrayList<String>();
			
			while (resultSet.next()){
				String name = resultSet.getString("USER_ID");
				users.add(name);
			}
			
			return users.toArray(new String[users.size()]);
		} catch (SQLException e){
			e.printStackTrace();
			return new String[0];
		}
	}
	
	public static Account getAccount(long accountNo) throws SQLException {

		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet =statement.executeQuery("SELECT ACCOUNT_NAME, BALANCE FROM ACCOUNTS WHERE ACCOUNT_ID = "+ accountNo +" "); /* BAD - user input should always be sanitized */

		ArrayList<Account> accounts = new ArrayList<Account>(3);
		while (resultSet.next()){
			String name = resultSet.getString("ACCOUNT_NAME");
			double balance = resultSet.getDouble("BALANCE"); 
			Account newAccount = new Account(accountNo, name, balance);
			accounts.add(newAccount);
		}
		
		if (accounts.size()==0)
			return null;
		
		return accounts.get(0);
	}

	public static Stock getStock(String stockNo) throws SQLException {
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet =statement.executeQuery("SELECT STOCK_NAME FROM STOCKS WHERE STOCK_ID = '"+ stockNo +"' "); /* BAD - user input should always be sanitized */

		ArrayList<Stock> stocks = new ArrayList(3);
		while (resultSet.next()) {
			String name = resultSet.getString("STOCK_NAME");
			//double price = resultSet.getDouble("PRICE");
			Stock newStock = new Stock(stockNo, name);
			stocks.add(newStock);
		}

		if (stocks.size()==0) {
			return null;
		}

		return stocks.get(0);
	}

	public static Index getIndex(String indexNo) throws SQLException {
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet =statement.executeQuery("SELECT INDEX_NAME FROM INDEXES WHERE INDEX_ID = '"+ indexNo +"' "); /* BAD - user input should always be sanitized */

		ArrayList<Index> indexes = new ArrayList(3);
		while (resultSet.next()) {
			String name = resultSet.getString("INDEX_NAME");
			//double price = resultSet.getDouble("PRICE");
			Index newIndex = new Index(indexNo, name);
			indexes.add(newIndex);
		}

		if (indexes.size()==0) {
			return null;
		}

		return indexes.get(0);
	}

	public static String addAccount(String username, String acctType) {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			statement.execute("INSERT INTO ACCOUNTS (USERID,ACCOUNT_NAME,BALANCE) VALUES ('"+username+"','"+acctType+"'," + init_balance + ")");
			return null;
		} catch (SQLException e){
			return e.toString();
		}
	}
	
	public static String addSpecialUser(String username, String password, String firstname, String lastname) {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			statement.execute("INSERT INTO SPECIAL_CUSTOMERS (USER_ID,PASSWORD,FIRST_NAME,LAST_NAME,ROLE) VALUES ('"+username+"','"+password+"', '"+firstname+"', '"+lastname+"','user')");
			return null;
		} catch (SQLException e){
			return e.toString();
			
		}
	}
	
	public static String addUser(String username, String password, String firstname, String lastname) {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			statement.execute("INSERT INTO PEOPLE (USER_ID,PASSWORD,FIRST_NAME,LAST_NAME,ROLE) VALUES ('"+username+"','"+password+"', '"+firstname+"', '"+lastname+"','user')");
			return null;
		} catch (SQLException e){
			return e.toString();
			
		}
	}

	public static String addStock(String symbol, String stockName) {
		try {
			Connection connection = getConnection();

			String query = ("INSERT INTO STOCKS (STOCK_ID,STOCK_NAME) VALUES (?,?)");
			PreparedStatement ps = connection.prepareStatement(query);

			stockName.replaceAll("'", "\'");

			ps.setString(1,symbol);
			ps.setString(2,stockName);
			ps.executeUpdate();
			return null;
		} catch (SQLException e) {
			return e.toString();
		}
	}
	
	public static String changePassword(String username, String password) {
		try {
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			statement.execute("UPDATE PEOPLE SET PASSWORD = '"+ password +"' WHERE USER_ID = '"+username+"'");
			return null;
		} catch (SQLException e){
			return e.toString();
		}
	}

	
	public static long storeFeedback(String name, String email, String subject, String comments) {
		try{ 
			Connection connection = getConnection();
			Statement statement = connection.createStatement();
			statement.execute("INSERT INTO FEEDBACK (NAME,EMAIL,SUBJECT,COMMENTS) VALUES ('"+name+"', '"+email+"', '"+subject+"', '"+comments+"')", Statement.RETURN_GENERATED_KEYS);
			ResultSet rs= statement.getGeneratedKeys();
			long id = -1;
			if (rs.next()){
				id = rs.getLong(1);
			}
			return id;
		} catch (SQLException e){
			Log4BigBucks.getInstance().logError(e.getMessage());
			return -1;
		}
	}

	/**
	 * Functions for historical data
	 */


	public static String _timestampToDateString(long timestamp){
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date date = new Date();
		date.setTime(timestamp * 1000L);
		return df.format(date);
	}

	public static String _timestampToTimeString(long timestamp){
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		java.util.Date date = new Date();
		date.setTime(timestamp * 1000L);
		return df.format(date);
	}

//	public static void setQuote(String symbol, ArrayList<Long> timestamps, ArrayList<Double> open,
//								ArrayList<Double> high, ArrayList<Double> low,
//								ArrayList<Double> close, ArrayList<Double> volume,
//								ArrayList<Double> adjClose) throws SQLException {
//		Connection connection = getConnection();
//		Statement statement = connection.createStatement();
//		for (int i=0; i<timestamps.size(); i++){
//			String datetime = _timestampToDateString(timestamps.get(i));
//
//			if (datetime.equals(APIUtil._getToday()) && ((APIUtil._getHour() < 16) || (APIUtil._getHour() == 16 && APIUtil._getMinute() <= 10)) ){
//				continue;
//			}
//
//			double simple_return = 0;
//			double LastPrice = 0;
//			double CurrPrice = 0;
//			if(i==0) {
//				LastPrice = getLastAdjCloseByDate(symbol, datetime);
//				try {
//					CurrPrice = adjClose.get(i);
//				}
//				catch(Exception e){
//					adjClose.set(i, LastPrice);
//					CurrPrice = LastPrice;
//				}
//			}
//			else {
//
//				try {
//					LastPrice = adjClose.get(i-1);
//				}
//				catch(Exception e){
//					LastPrice = 0;
//				}
//				try {
//					CurrPrice = adjClose.get(i);
//				}
//				catch(Exception e){
//					adjClose.set(i, LastPrice);
//					CurrPrice = LastPrice;
//				}
//			}
//			if (LastPrice > 0 && CurrPrice > 0) {
//				simple_return = (CurrPrice - LastPrice) / LastPrice;
//			}
//
//			try {
//				statement.execute("INSERT INTO HISTORY_DATA (STOCKID, DATETIME, PRICE_OPEN, PRICE_HIGH, PRICE_LOW, PRICE_CLOSE, PRICE_ADJ_CLOSE, VOLUME, SIMPLE_RETURN)" +
//						" VALUES ( '" + symbol + "','" + datetime + "'," + open.get(i) + ", " + high.get(i) + "," + low.get(i) + "," + close.get(i) + "," + adjClose.get(i) + "," + volume.get(i) + "," + simple_return + ")");
//			}
//			catch (SQLException e) {
//				statement.execute("UPDATE HISTORY_DATA SET PRICE_OPEN = "+ open.get(i) + ", PRICE_HIGH ="+close.get(i)+", PRICE_LOW ="+ low.get(i)+", PRICE_CLOSE="+ close.get(i)+", PRICE_ADJ_CLOSE=" +adjClose.get(i)+" WHERE (STOCKID= '" + symbol +"' AND DATETIME ='"+datetime+"')");
//			}
//		}
//	}

	static public String calendarToDate(Calendar calendar) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(calendar.getTime());
	}

	public static void setQuote(String symbol, List<HistoricalQuote> history) throws SQLException {
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		for (int i=0; i<history.size(); ++i){
			String datetime = calendarToDate(history.get(i).getDate());

//			if (datetime.equals(APIUtil._getToday()) && (((APIUtil._getHour() < 16) || (APIUtil._getHour() == 16 && APIUtil._getMinute() < 1))) ){
//				continue;
//			}

			double simple_return = 0;
			double LastPrice = 0;
			double CurrPrice = 0;
			if(i==0) {
				LastPrice = getLastAdjCloseByDate(symbol, datetime);
				try {
					CurrPrice = history.get(i).getAdjClose().doubleValue();
				}
				catch(Exception e){
					history.get(i).setAdjClose(new BigDecimal(LastPrice));
					CurrPrice = LastPrice;
				}
			}
			else {

				try {
					LastPrice = history.get(i-1).getAdjClose().doubleValue();
				}
				catch(Exception e){
					LastPrice = 0;
				}
				try {
					CurrPrice = history.get(i).getAdjClose().doubleValue();
				}
				catch(Exception e){
					history.get(i).setAdjClose(new BigDecimal(LastPrice));
					CurrPrice = LastPrice;
				}
			}
			if (LastPrice > 0 && CurrPrice > 0) {
				simple_return = (CurrPrice - LastPrice) / LastPrice;
			}

			HistoricalQuote quote = history.get(i);
			try {
				statement.execute("INSERT INTO HISTORY_DATA (STOCKID, DATETIME, PRICE_OPEN, PRICE_HIGH, PRICE_LOW, PRICE_CLOSE, PRICE_ADJ_CLOSE, VOLUME, SIMPLE_RETURN)" +
						" VALUES ( '" + symbol + "','" + datetime + "'," + quote.getOpen() + ", " + quote.getHigh() + "," + quote.getLow() + "," + quote.getClose() + "," + quote.getAdjClose() + "," + quote.getVolume() + "," + simple_return + ")");
			}
			catch (SQLException e) {
				statement.execute("UPDATE HISTORY_DATA SET PRICE_OPEN = "+ quote.getOpen() + ", PRICE_HIGH ="+quote.getHigh()+", PRICE_LOW ="+ quote.getLow()+", PRICE_CLOSE="+ quote.getClose()+", PRICE_ADJ_CLOSE=" +quote.getAdjClose() +", VOLUME=" +quote.getVolume()+" WHERE (STOCKID= '" + symbol +"' AND DATETIME ='"+datetime+"')");
			}
		}
	}

	public static ArrayList<String> getQuote(String symbol, String columnName) throws SQLException {
		columnName = columnName.toUpperCase();
		ArrayList<String> result = new ArrayList<>();

		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet =
				statement.executeQuery("SELECT " + columnName + " FROM HISTORY_DATA WHERE STOCKID = '" + symbol + "'");

		while (resultSet.next()) {
			result.add(resultSet.getString(columnName));
		}

		return result;
	}

	public static double getLastAdjCloseByDate(String symbol, String date) throws SQLException {

		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet =
				statement.executeQuery("SELECT PRICE_ADJ_CLOSE FROM HISTORY_DATA WHERE STOCKID = '" + symbol + "' and DATETIME<'"+ date + "' ORDER BY DATETIME DESC");

		if (resultSet.next()) {
			return resultSet.getDouble("PRICE_ADJ_CLOSE");
		}

		return -1;
	}

	public static double getLastAdjClose(String symbol) throws SQLException {

		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet =
				statement.executeQuery("SELECT PRICE_ADJ_CLOSE FROM HISTORY_DATA WHERE STOCKID = '" + symbol + "'" + "ORDER BY DATETIME DESC");

		if (resultSet.next()) {
			return resultSet.getDouble("PRICE_ADJ_CLOSE");
		}

		return -1;
	}

	public static void removeQuote(String symbol) throws SQLException{
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		statement.executeUpdate("DELETE FROM HISTORY_DATA WHERE STOCKID=" + symbol);
	}

	public static String getLastQuoteDatetime(String symbol) throws SQLException{
		String result = "";

		Connection connection = getConnection();
		Statement statement = connection.createStatement();

		ResultSet resultSet =
				statement.executeQuery("SELECT MAX(DATETIME) FROM HISTORY_DATA WHERE STOCKID = '" + symbol + "'");
		if (resultSet.next()){
			result += resultSet.getString(1);
		}
		return result;
	}

	public static ArrayList<String> getAllQuoteSymbols() throws SQLException{
		ArrayList<String> result = new ArrayList<>();

		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet =
				statement.executeQuery("SELECT DISTINCT STOCKID FROM HISTORY_DATA");

		while (resultSet.next()) {
			result.add(resultSet.getString(1));
		}

		return result;
	}


	public static void printAllRows(String tableName) throws SQLException {
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * from " + tableName);
		ResultSetMetaData rsmd = resultSet.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		while (resultSet.next()) {
			for (int i = 1; i <= columnsNumber; i++) {
				if (i > 1) System.out.print(",  ");
				String columnValue = resultSet.getString(i);
				System.out.print(rsmd.getColumnName(i) + " " + columnValue);
			}
			System.out.println("");
		}
	}
}