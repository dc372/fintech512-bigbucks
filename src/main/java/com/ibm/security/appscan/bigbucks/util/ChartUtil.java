package com.ibm.security.appscan.bigbucks.util;

import com.ibm.security.appscan.bigbucks.model.Index;
import com.ibm.security.appscan.bigbucks.model.Stock;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.statistics.HistogramDataset;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleInsets;
import org.jfree.util.ShapeUtilities;

import java.awt.*;
import java.io.IOException;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

/**
 * An example of a time series chart create using JFreeChart.  For the most
 * part, default settings are used, except that the renderer is modified to
 * show filled shapes (as well as lines) at each data point.
 */
public class ChartUtil {

    private static final long serialVersionUID = 1L;

    private String StockId;

    /**
     * Constructs a new application frame.
     *
     * @param StockId the frame title.
     */
    public ChartUtil(String StockId) throws SQLException, IOException, ParseException, InterruptedException {
        this.StockId = StockId;
        //ChartPanel chartPanel = (ChartPanel) createDemoPanel(StockId);
        //chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        //setContentPane(chartPanel);
    }

    /**
     * A demonstration application showing how to create a simple time series
     * chart.  This example uses monthly data.
     *
     * @param title  the frame title.
     */


    /**
     * Creates a chart.
     *
     * @param StockId
     *
     * @return A chart.
     */
    public static JFreeChart createChart(String StockId) throws SQLException, IOException, ParseException, InterruptedException {
        XYDataset dataset = createDataset(StockId);

        String title = "";

        try {
            title=Stock.getStock(StockId).getStockName();
        }
        catch (Exception e) {
            title = Index.getIndex(StockId).getIndexName();
        }

        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                title,  // title
                "Date",             // x-axis label
                "Price",   // y-axis label
                dataset,false,false,false);

        chart.setBackgroundPaint(Color.WHITE);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
        plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);
        plot.setOutlineVisible(false);

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;

            //renderer.setDefaultShapesVisible(true);
            //renderer.setDefaultShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
        }

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        return chart;

    }

    /**
     * Creates a dataset, consisting of two series of monthly data.
     *
     * @return The dataset.
     */
    private static XYDataset createDataset(String StockId) throws SQLException, IOException, ParseException, InterruptedException {

        ArrayList<String> datetime = APIUtil.getHistorical(StockId , "datetime");
        ArrayList<String> price_adj_close = APIUtil.getHistorical(StockId, "price_adj_close");

        TimeSeries s1 = new TimeSeries(StockId);
        //s1.add(new Month(2, 2001), 181.8);

        for (int i=0; i< datetime.size(); ++i) {
            String[] dateArr = datetime.get(i).split("-");
            double price = 0;
            try {
                price = Double.parseDouble(price_adj_close.get(i));  //Double will treat null as null, while double treat null as 0;
            }
            catch (Exception e) {
                price = 0;
            }
            /*
            Day(int day, int month, int year)
                Constructs a new one day time period.
             */
            s1.add(new Day(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[0])), price);
        }

        TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.addSeries(s1);

        return dataset;

    }

    public static JFreeChart createReturnChart(String StockId) throws SQLException, IOException, ParseException, InterruptedException {
        XYDataset dataset = createReturnDataset(StockId);

        JFreeChart chart = ChartFactory.createScatterPlot(
                "Daily simple return of " + StockId,  // title
                "Date",             // x-axis label
                "Return",   // y-axis label
                dataset, PlotOrientation.VERTICAL ,false,false,false);

        chart.setBackgroundPaint(Color.WHITE);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
        //plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(false);
        plot.setRangeCrosshairVisible(true);
        plot.setRangeCrosshairPaint(Color.BLACK);
        plot.setOutlineStroke(new BasicStroke());
        plot.setOutlineVisible(false);

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;

            //renderer.setDefaultShapesVisible(true);
            //renderer.setDefaultShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
            renderer.setUseFillPaint(true);
            renderer.setUseOutlinePaint(true);
            renderer.setSeriesOutlinePaint(0,Color.gray);
            renderer.setBasePaint(Color.gray);
            renderer.setSeriesFillPaint(0,Color.gray);
            renderer.setSeriesShape(0, ShapeUtilities.createDiamond(2.0f));
        }

//        DateAxis axis = (DateAxis) plot.getDomainAxis();
//        axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        DateAxis xAxis = new DateAxis("Date");
        xAxis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));
        plot.setDomainAxis(xAxis);

        NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
        yAxis.setNumberFormatOverride(NumberFormat.getPercentInstance());

        return chart;

    }

    /**
     * Creates a dataset, consisting of two series of monthly data.
     *
     * @return The dataset.
     */
    private static XYDataset createReturnDataset(String StockId) throws SQLException, IOException, ParseException, InterruptedException {

        ArrayList<String> datetime = APIUtil.getHistorical(StockId , "datetime");
        ArrayList<String> price_adj_close = APIUtil.getHistorical(StockId, "simple_return");

        XYSeries s1 = new XYSeries(StockId);
        //s1.add(new Month(2, 2001), 181.8);

        for (int i=0; i< datetime.size(); ++i) {
            String[] dateArr = datetime.get(i).split("-");
            double price = 0;
            try {
                price = Double.parseDouble(price_adj_close.get(i));  //Double will treat null as null, while double treat null as 0;
            }
            catch (Exception e) {
                price = 0;
            }
            /*
            Day(int day, int month, int year)
                Constructs a new one day time period.
             */
            s1.add(new Day(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[0])).getMiddleMillisecond(), price);
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(s1);

        return dataset;

    }

    public static JFreeChart createReturn2DaysChart(String StockId) throws SQLException, IOException, ParseException, InterruptedException {
        XYDataset dataset = createReturn2DaysDataset(StockId);

        JFreeChart chart = ChartFactory.createScatterPlot(
                "Two consecutive trading days of " + StockId,  // title
                "Return (-1)",             // x-axis label
                "Return",   // y-axis label
                dataset, PlotOrientation.VERTICAL ,false,false,false);

        chart.setBackgroundPaint(Color.WHITE);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        //plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(true);
        plot.setDomainCrosshairPaint(Color.BLACK);
        plot.setRangeCrosshairPaint(Color.BLACK);
        plot.setDomainGridlinesVisible(true);
        plot.setRangeGridlinesVisible(true);
        plot.setDomainGridlinePaint(Color.GRAY);
        plot.setRangeGridlinePaint(Color.GRAY);
        plot.setRangeMinorGridlinesVisible(true);
        plot.setDomainMinorGridlinesVisible(true);
        plot.setRangeMinorGridlinePaint(Color.lightGray);
        plot.setDomainMinorGridlinePaint(Color.lightGray);

        plot.setOutlineVisible(true);
        plot.setDomainGridlineStroke(new BasicStroke());
        plot.setRangeGridlineStroke(new BasicStroke());

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;

            //renderer.setDefaultShapesVisible(true);
            //renderer.setDefaultShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
            renderer.setUseFillPaint(true);
            renderer.setUseOutlinePaint(true);
            renderer.setSeriesOutlinePaint(0,Color.gray);
            renderer.setBasePaint(Color.gray);
            renderer.setSeriesFillPaint(0,Color.gray);
            renderer.setSeriesShape(0, ShapeUtilities.createDiamond(2.0f));
        }

//        DateAxis axis = (DateAxis) plot.getDomainAxis();
//        axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
        xAxis.setNumberFormatOverride(NumberFormat.getPercentInstance());

        NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
        yAxis.setNumberFormatOverride(NumberFormat.getPercentInstance());

        return chart;

    }

    /**
     * Creates a dataset, consisting of two series of monthly data.
     *
     * @return The dataset.
     */
    private static XYDataset createReturn2DaysDataset(String StockId) throws SQLException, IOException, ParseException, InterruptedException {

        ArrayList<String> price_adj_close = APIUtil.getHistorical(StockId, "simple_return");

        XYSeries s1 = new XYSeries(StockId);
        //s1.add(new Month(2, 2001), 181.8);

        for (int i=0; i< price_adj_close.size()-1; ++i) {
            double price = 0;
            double price0 = 0;
            try {
                price = Double.parseDouble(price_adj_close.get(i+1));  //Double will treat null as null, while double treat null as 0;
            }
            catch (Exception e) {
                price = 0;
            }
            try {
                price0 = Double.parseDouble(price_adj_close.get(i));  //Double will treat null as null, while double treat null as 0;
            }
            catch (Exception e) {
                price0 = 0;
            }

            s1.add(price0, price);
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(s1);

        return dataset;

    }

    public static JFreeChart createHistogramChart(String StockId) throws SQLException, IOException, ParseException, InterruptedException {
        HistogramDataset dataset = createHistogramDataset(StockId);

        JFreeChart chart = ChartFactory.createHistogram(
                "Histogram of simple return for " + StockId,  // title
                "Return bin",             // x-axis label
                "Frequency",   // y-axis label
                dataset, PlotOrientation.VERTICAL ,false,false,false);

        chart.setBackgroundPaint(Color.WHITE);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        //plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(true);
        plot.setRangeCrosshairVisible(false);
        plot.setDomainCrosshairPaint(Color.BLACK);
        plot.setRangeCrosshairPaint(Color.BLACK);

        plot.setOutlineVisible(false);
        plot.setDomainGridlineStroke(new BasicStroke());
        plot.setRangeGridlineStroke(new BasicStroke());

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;

            //renderer.setDefaultShapesVisible(true);
            //renderer.setDefaultShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
            renderer.setUseFillPaint(true);
            renderer.setUseOutlinePaint(true);
            renderer.setSeriesOutlinePaint(0,Color.gray);
            renderer.setBasePaint(Color.gray);
            renderer.setSeriesFillPaint(0,Color.gray);
            renderer.setSeriesShape(0, ShapeUtilities.createDiamond(2.0f));
        }

//        DateAxis axis = (DateAxis) plot.getDomainAxis();
//        axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
        xAxis.setNumberFormatOverride(NumberFormat.getPercentInstance());

        return chart;

    }

    /**
     * Creates a dataset, consisting of two series of monthly data.
     *
     * @return The dataset.
     */
    private static HistogramDataset createHistogramDataset(String StockId) throws SQLException, IOException, ParseException, InterruptedException {

        ArrayList<String> price_adj_close = APIUtil.getHistorical(StockId, "simple_return");


        ArrayList<Double> s1 = new ArrayList<Double>();

        for (int i=0; i< price_adj_close.size(); ++i) {
            double price = 0;
            try {
                price = Double.parseDouble(price_adj_close.get(i));  //Double will treat null as null, while double treat null as 0;
            }
            catch (Exception e) {
                price = 0;
            }

            s1.add(price);
        }

        HistogramDataset dataset = new HistogramDataset();
        dataset.addSeries(StockId, s1.stream().mapToDouble(Double::doubleValue).toArray(), (int) ((Collections.max(s1).doubleValue()- Collections.min(s1).doubleValue())*50));
        return dataset;

    }


    public static JFreeChart createCumChart(String StockId) throws SQLException, IOException, ParseException, InterruptedException {
        XYDataset dataset = createCumDataset(StockId);

        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                StockId+ " and S&P 500 cumulative returns",  // title
                "Date",             // x-axis label
                "Relative price",   // y-axis label
                dataset,true,false,false);

        chart.setBackgroundPaint(Color.WHITE);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
        //plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(false);
        plot.setRangeCrosshairVisible(true);
        plot.setRangeCrosshairPaint(Color.BLACK);
        plot.setOutlineStroke(new BasicStroke());
        plot.setOutlineVisible(false);

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;

            //renderer.setDefaultShapesVisible(true);
            //renderer.setDefaultShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
        }

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        return chart;

    }

    private static ArrayList<String> _cutArray(ArrayList<String> array, int size) {
        if (size>array.size())
            return array;
        ArrayList<String> result = new ArrayList<>();
        for (int i=(array.size() - size); i< array.size(); i++){
            result.add(array.get(i));
        }
        return result;
    }

    /**
     * Creates a dataset, consisting of two series of monthly data.
     *
     * @return The dataset.
     */
    private static XYDataset createCumDataset(String StockId) throws SQLException, IOException, ParseException, InterruptedException {

        ArrayList<String> datetime = APIUtil.getHistorical(StockId , "datetime");
        ArrayList<String> stockReturn = APIUtil.getHistorical(StockId, "simple_return");
        ArrayList<String> indexReturn = APIUtil.getHistorical("^GSPC", "simple_return");

        int size = Math.min(stockReturn.size(), indexReturn.size());
        if (stockReturn.size()>size) {
            stockReturn = _cutArray(stockReturn,size);
            datetime = _cutArray(datetime,size);
        }
        if (indexReturn.size()>size) {
            indexReturn = _cutArray(indexReturn,size);
        }

        TimeSeries s1 = new TimeSeries(StockId);
        TimeSeries s2 = new TimeSeries("S&P 500");

        double cumStock = 1;
        double cumIndex = 1;

        for (int i=0; i< datetime.size(); ++i) {
            String[] dateArr = datetime.get(i).split("-");
            double s_r = 0;
            double i_r = 0;
            try {
                s_r = Double.parseDouble(stockReturn.get(i));  //Double will treat null as null, while double treat null as 0;
            }
            catch (Exception e) {
                s_r = 0;
            }
            try {
                i_r = Double.parseDouble(indexReturn.get(i));  //Double will treat null as null, while double treat null as 0;
            }
            catch (Exception e) {
                i_r = 0;
            }
            cumStock *= 1+s_r;
            cumIndex *= 1+i_r;
            /*
            Day(int day, int month, int year)
                Constructs a new one day time period.
             */
            s1.add(new Day(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[0])), cumStock);
            s2.add(new Day(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[0])), cumIndex);
        }

        TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.addSeries(s1);
        dataset.addSeries(s2);

        return dataset;

    }

    public static JFreeChart createCompareChart(String StockId) throws SQLException, IOException, ParseException, InterruptedException {
        XYDataset dataset = createCompareDataset(StockId);

        JFreeChart chart = ChartFactory.createTimeSeriesChart(
                "Daily return change for "+ StockId+ " and S&P 500",  // title
                "Date",             // x-axis label
                "Daily returns (%)",   // y-axis label
                dataset,true,false,false);

        chart.setBackgroundPaint(Color.WHITE);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.LIGHT_GRAY);
        //plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(false);
        plot.setRangeCrosshairVisible(true);
        plot.setRangeCrosshairStroke(new BasicStroke());
        plot.setRangeCrosshairPaint(Color.BLACK);
        plot.setOutlineStroke(new BasicStroke());
        plot.setOutlineVisible(false);

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;

            //renderer.setDefaultShapesVisible(true);
            //renderer.setDefaultShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
        }

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

        NumberAxis xAxis = (NumberAxis) plot.getRangeAxis();
        xAxis.setNumberFormatOverride(NumberFormat.getPercentInstance());

        return chart;

    }

    /**
     * Creates a dataset, consisting of two series of monthly data.
     *
     * @return The dataset.
     */
    private static XYDataset createCompareDataset(String StockId) throws SQLException, IOException, ParseException, InterruptedException {

        ArrayList<String> datetime = APIUtil.getHistorical(StockId , "datetime");
        ArrayList<String> stockReturn = APIUtil.getHistorical(StockId, "simple_return");
        ArrayList<String> indexReturn = APIUtil.getHistorical("^GSPC", "simple_return");

        int size = Math.min(stockReturn.size(), indexReturn.size());
        if (stockReturn.size()>size) {
            stockReturn = _cutArray(stockReturn,size);
            datetime = _cutArray(datetime,size);
        }
        if (indexReturn.size()>size) {
            indexReturn = _cutArray(indexReturn,size);
        }

        TimeSeries s1 = new TimeSeries(StockId);
        TimeSeries s2 = new TimeSeries("S&P 500");

        for (int i=0; i< datetime.size(); ++i) {
            String[] dateArr = datetime.get(i).split("-");
            double s_r = 0;
            double i_r = 0;
            try {
                s_r = Double.parseDouble(stockReturn.get(i));  //Double will treat null as null, while double treat null as 0;
            }
            catch (Exception e) {
                s_r = 0;
            }
            try {
                i_r = Double.parseDouble(indexReturn.get(i));  //Double will treat null as null, while double treat null as 0;
            }
            catch (Exception e) {
                i_r = 0;
            }
            /*
            Day(int day, int month, int year)
                Constructs a new one day time period.
             */
            s1.add(new Day(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[0])), s_r);
            s2.add(new Day(Integer.parseInt(dateArr[2]), Integer.parseInt(dateArr[1]), Integer.parseInt(dateArr[0])), i_r);
        }

        TimeSeriesCollection dataset = new TimeSeriesCollection();
        dataset.addSeries(s1);
        dataset.addSeries(s2);

        return dataset;

    }


    private static double _calBeta(ArrayList<Double> xVariable, ArrayList<Double> yVariable){
        double xMean = xVariable.stream().mapToDouble(d -> d).average().orElse(0.0);
        double yMean = yVariable.stream().mapToDouble(d -> d).average().orElse(0.0);
        double nominator = 0;
        double denominator = 0;
        for (int i=0; i<xVariable.size(); i++){
            nominator += (xVariable.get(i) - xMean) * (yVariable.get(i) - yMean);
            denominator += (xVariable.get(i) - xMean) * (xVariable.get(i) - xMean);
        }
        return nominator / denominator;
    }

    private static double _calAlpha(ArrayList<Double> xVariable, ArrayList<Double> yVariable,
                                    double beta){
        double xMean = xVariable.stream().mapToDouble(d -> d).average().orElse(0.0);
        double yMean = yVariable.stream().mapToDouble(d -> d).average().orElse(0.0);
        return yMean - (beta * xMean);
    }

    private static ArrayList<Double> _findFittedValues(ArrayList<Double> xVariable,
                                                       ArrayList<Double> yVariable){
        double beta = _calBeta(xVariable, yVariable);
        double alpha = _calAlpha(xVariable, yVariable, beta);
        ArrayList<Double> fitted = new ArrayList<>();
        for (Double x : xVariable){
            fitted.add(x * beta + alpha);
        }
        return fitted;
    }

    private static ArrayList<Double> _stringListToDouble(ArrayList<String> list){
        ArrayList<Double> result = new ArrayList<>();
        for (String s : list) {
            double temp = 0;
            try {
                temp = Double.parseDouble(s);
            }
            catch (Exception e) {
                temp=0;
            }
            result.add(temp);
        }
        return result;
    }

    public static JFreeChart createCAPMChart(String StockId) throws SQLException, IOException, ParseException, InterruptedException {
        XYDataset dataset = createCAPMDataset(StockId);

        JFreeChart chart = ChartFactory.createXYLineChart(
                "CAPM for "+ StockId+ " and S&P 500",  // title
                "S&P 500",             // x-axis label
                StockId,   // y-axis label
                dataset,
                PlotOrientation.VERTICAL,true,true,false);

        chart.setBackgroundPaint(Color.WHITE);

        XYPlot plot = (XYPlot) chart.getPlot();
        plot.setBackgroundPaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.WHITE);
        plot.setRangeGridlinePaint(Color.WHITE);
        //plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
        plot.setDomainCrosshairVisible(false);
        plot.setRangeCrosshairVisible(false);
        plot.setOutlineVisible(true);
        plot.setOutlineStroke(new BasicStroke());

        XYItemRenderer r = plot.getRenderer();
        if (r instanceof XYLineAndShapeRenderer) {
            XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;

            //renderer.setDefaultShapesVisible(true);
            //renderer.setDefaultShapesFilled(true);
            renderer.setDrawSeriesLineAsPath(true);
            renderer.setUseFillPaint(true);
            renderer.setUseOutlinePaint(true);
            renderer.setSeriesOutlinePaint(0,Color.gray);
            renderer.setBasePaint(Color.gray);
            renderer.setSeriesFillPaint(0,Color.gray);
            renderer.setSeriesShape(0, ShapeUtilities.createDiamond(2.0f));

            // "1" is the line plot
            renderer.setSeriesLinesVisible(1, true);
            renderer.setSeriesShapesVisible(1, false);

            // "0" is the scatter plot
            renderer.setSeriesLinesVisible(0, false);
            renderer.setSeriesShapesVisible(0, true);
        }

        NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
        xAxis.setNumberFormatOverride(NumberFormat.getPercentInstance());

        NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
        yAxis.setNumberFormatOverride(NumberFormat.getPercentInstance());

        return chart;

    }

    public static XYSeriesCollection createCAPMDataset(String StockId) throws SQLException,
            IOException, ParseException, InterruptedException {
        ArrayList<String> stockReturn = APIUtil.getHistorical(StockId, "simple_return");
        ArrayList<String> indexReturn = APIUtil.getHistorical("^GSPC", "simple_return");

        int size = Math.min(stockReturn.size(), indexReturn.size());
        if (stockReturn.size()>size) {
            stockReturn = _cutArray(stockReturn,size);
        }
        if (indexReturn.size()>size) {
            indexReturn = _cutArray(indexReturn,size);
        }

        ArrayList<Double> db_stockReturn = _stringListToDouble(stockReturn);
        ArrayList<Double> db_indexReturn = _stringListToDouble(indexReturn);
        ArrayList<Double> fitted = _findFittedValues(db_indexReturn, db_stockReturn);

        XYSeries s1 = new XYSeries("scattered");
        XYSeries s2 = new XYSeries("fitted");

        for (int i=0; i< db_indexReturn.size(); ++i) {
            s1.add(db_indexReturn.get(i), db_stockReturn.get(i));
            s2.add(db_indexReturn.get(i), fitted.get(i));
        }

        XYSeriesCollection dataset = new XYSeriesCollection();
        dataset.addSeries(s1);
        dataset.addSeries(s2);

        return dataset;
    }


//    public static String getImg(HttpSession session, JspWriter out, String StockId) {
//        String filename = null;
//        try {
//            JFreeChart chart = createChart(StockId);
//            PrintWriter  pw=new PrintWriter(out);
//            StandardEntityCollection sec = new StandardEntityCollection();
//            ChartRenderingInfo info = new ChartRenderingInfo(sec);
//
//            filename= ServletUtilities.saveChartAsJPEG(chart,500, 270,null, session);
//            ChartUtilities.writeImageMap(pw, filename,info, true);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return filename;
//    }

    /**
     * Creates a panel for the demo (used by SuperDemo.java).
     *
     * @return A panel.
     */
    /*public static JPanel createDemoPanel(String StockId) throws SQLException, IOException, ParseException, InterruptedException {
        JFreeChart chart = createChart(StockId);
        ChartPanel panel = new ChartPanel(chart, false);
        panel.setFillZoomRectangle(true);
        panel.setMouseWheelEnabled(true);
        return panel;
    }*/

    /**
     * Starting point for the demonstration application.
     *
     * @param args  ignored.
     */
    public static void main(String[] args) throws SQLException, IOException, ParseException, InterruptedException {
        String StockId = "MSFT";
        JFreeChart chart = createChart(StockId);

        //ChartUtil demo = new ChartUtil("MSFT");
        //UIUtilities.centerFrameOnScreen(demo);
        //demo.setVisible(true);
    }

}