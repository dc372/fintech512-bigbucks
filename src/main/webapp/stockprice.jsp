<%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 3/16/22
  Time: 5:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<jsp:include page="header.jspf"/>

<div id="wrapper" style="width: 99%;">
    <jsp:include page="/toc.jspf"/>
        <td valign="top" colspan="3" class="bb">
            <%@page import="com.ibm.security.appscan.bigbucks.model.Stock"%>
            <%@page import="java.text.SimpleDateFormat"%>
            <%@page import="java.text.NumberFormat"%>
            <%@page import="java.text.DecimalFormat"%>
            <%@page import="java.util.ArrayList"%><div class="fl" style="width: 99%;">

            <div class="fl" style="width: 99%;">

                <h1>Pick Stock</h1>

                <form id="Form1" method="get" action="showStock">
                    <select size="1" name="listStocks" id="listStocks">

                        <%
                            Stock[] stocks =  Stock.getStocks();
                            for (Stock stock: stocks){
                                out.println("<option value=\""+stock.getStockId()+"\">(" + stock.getStockId() + ") " + stock.getStockName() + "</option>");
                            }
                            //double dblPrice = Account.getAccount(paramName).getBalance();
                            //String format = (dblBalance<1)?"$0.00":"$.00";
                            //String balance = new DecimalFormat(format).format(dblBalance);
                        %>
                    </select>
                    <input type="submit" id="btnGetAccount" Value="Select Stock">
                </form>
            </div>
</div>

<jsp:include page="footer.jspf"/>
