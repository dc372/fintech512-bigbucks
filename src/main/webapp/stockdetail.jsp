<%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 3/17/22
  Time: 3:46 AM
  To change this template use File | Settings | File Templates.
--%>
<jsp:include page="/header.jspf"/>

<% boolean loggedIn = request.getSession().getAttribute("user") != null; %>

<style>
    .tradeBtn {
        background-color: blue;
        border: none;
        border-radius: 12px;
        border: 1.5px solid blue;
        color: white;
        font-size: 15px;
        background: blue;
        display: inline-block;
        padding: 13px 28px 13px 28px;

        margin: 10px;

        -webkit-transition-duration: 0.4s; /* Safari */
        transition-duration: 0.4s;
    }

    .tradeBtn:hover {
        background: white;
        color: black;
    }
</style>

<div id="wrapper" style="width: 99%;">
    <jsp:include page="toc.jspf"/>
    <td valign="top" colspan="3" class="bb">
        <%@page import="com.ibm.security.appscan.bigbucks.model.Stock"%>
        <%@page import="java.text.SimpleDateFormat"%>
        <%@page import="java.text.NumberFormat"%>
        <%@page import="java.text.DecimalFormat"%>
        <%@page import="java.util.ArrayList"%>
        <%@ page import="com.ibm.security.appscan.bigbucks.util.APIUtil" %>
        <%@ page import="com.ibm.security.appscan.bigbucks.util.DBUtil" %>
        <div class="fl" style="width: 99%;">

        <%
            ArrayList<Stock> stocks = new ArrayList<Stock>();
            java.lang.String paramName = request.getParameter("stockId");
            String stockName = paramName;

            for (Stock stock: Stock.getStocks()) {
                if (!String.valueOf(stock.getStockId()).equals(paramName))
                    stocks.add(stock);
                else {
                    stocks.add(0, stock);
                    stockName = "(" + stock.getStockId() + ") " +stock.getStockName();
                }
            }
        %>

        <!-- To modify account information do not connect to SQL source directly.  Make all changes
        through the admin page. -->

        <h1>Stock Detail - <%=stockName%></h1>

        <table width="700" border="0">
            <tr>
                <td colspan=2>
                    <table cellSpacing="0" cellPadding="1" width="100%" border="1">
                        <tr>
                            <th colSpan="2">
                                Balance Detail</th></tr>
                        <tr>
                            <th align="left" width="80%" height="26">
                                <form id="Form1" method="get" action="showStock">
                                    <select size="1" name="listStocks" id="listStocks">
                                        <%
                                            for (Stock stock: stocks){
                                                out.println("<option value=\""+stock.getStockId()+"\">(" + stock.getStockId() + ") " + stock.getStockName() + "</option>");
                                            }
                                            double dblPrice = APIUtil.getLastAdjClose(paramName);
                                            String closeDate = DBUtil.getLastQuoteDatetime(paramName);
                                            String format = (dblPrice<1)?"$0.00":"$.00";
                                            String price = new DecimalFormat(format).format(dblPrice);
                                        %>
                                    </select>
                                    <input type="submit" id="btnGetStock" Value="Select Stock">
                                </FORM>
                            </th>
                            <th align="middle" height="26">
                                Price
                            </th>
                        </tr>
                        <tr>
                            <td>Adjusted Closed Price as of <%=closeDate%>
                            </td>
                            <td align="right"><% out.println(price); %></td>
                        </tr>
                        <tr hidden>
                            <td>Current price
                            </td>
                            <td align="right">
                                <% out.println(price); %></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <a href = "<%=request.getContextPath()%><%=(!loggedIn)?"/login.jsp":"/bank/tradestock.jsp"%><%="?stockId="+paramName%>"> <button class="tradeBtn">Trade</button></a>
    </div>
    </td>
</div>

<jsp:include page="/footer.jspf"/>
