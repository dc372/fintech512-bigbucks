<%@ page import="com.ibm.security.appscan.bigbucks.util.ChartUtil" %>
<%@ page import="org.jfree.chart.servlet.ServletUtilities" %>
<%@ page import="com.ibm.security.appscan.bigbucks.model.Index" %><%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 4/3/22
  Time: 5:31 PM
  To change this template use File | Settings | File Templates.
--%>
<jsp:include page="/header.jspf"/>

<% boolean loggedIn = request.getSession().getAttribute("user") != null; %>

</style>

<div id="wrapper" style="width: 99%;">
    <jsp:include page="toc.jspf"/>
    <td valign="top" colspan="3" class="bb">
        <div class="fl" style="width: 99%;">
            <p>
                <%
                    for (Index index: Index.getIndexes()) {
                        String error = "";
                        String filename = null;
                        try {
                            filename = ServletUtilities.saveChartAsPNG(ChartUtil.createChart(index.getIndexId()), 500, 270, null);

                        } catch (Exception e) {
                            error = e.getMessage();
                        }
                        String graphURL = request.getContextPath() + "/servlet/DisplayChart?filename="   + filename;
                %>
                    <br>
                    <img src="<%=graphURL%>" border="0" alt="<%=error%>" usemap="#<%=filename %>"/>
                    </br>
                <%
                    }
                %>
            </p>
        </div>
    </td>
</div>

<jsp:include page="/footer.jspf"/>
