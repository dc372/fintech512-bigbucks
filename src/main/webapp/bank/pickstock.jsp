<%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 3/17/22
  Time: 11:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<jsp:include page="/header.jspf"/>

<%
    java.lang.String searchStr = (String)request.getParameter("wd");

    if (searchStr == null || searchStr.trim().length() == 0){
        searchStr = "";
    }
%>

<div id="wrapper" style="width: 99%;">
    <jsp:include page="membertoc.jspf"/>
    <td valign="top" colspan="3" class="bb">
        <%@page import="com.ibm.security.appscan.bigbucks.model.Stock"%>
        <%@page import="java.util.ArrayList"%>
        <%@ page import="com.ibm.security.appscan.bigbucks.util.APIUtil" %>
        <div class="fl" style="width: 99%;">

        <div class="fl" style="width: 99%;">

            <h1>Search Stocks</h1>

            <form id="Form1" method="get" action="searchStock">
                <!-- Original method to select stock;
                     Now we enable new features to search stocks through yahoo finance API
                <select size="1" name="listStocks" id="listStocks">

                    <% /*
                        Stock[] stocks =  Stock.getStocks();
                        for (Stock stock: stocks){
                            out.println("<option value=\""+stock.getStockId()+"\">(" + stock.getStockId() + ") " + stock.getStockName() + "</option>");
                        }*/
                    %>
                </select>
                -->
                <input width="5000" height="50" type="text" id="search-input" name="wd" value=<%=searchStr%>> <input type="submit" id="searchBtn" Value="Search">
            </form>
            <table id="results" style="display:<%=(searchStr.length() == 0)?"none":"block"%>">
                <tr>
                    <td colspan="2">
                        <b>Search Results for "<%=searchStr%>":</b>
                    </td>
                </tr>
                <%
                    ArrayList<Stock> resultSet = null;
                    if (searchStr.trim().length()>0) {
                        try {
                            resultSet = APIUtil.getSearch(searchStr);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        for (int i=0; i< resultSet.size(); ++i) {
                            java.lang.String stockId = resultSet.get(i).getStockId();
                            java.lang.String stockName = resultSet.get(i).getStockName();
                %>
                <tr><td><%=stockId%></td><td> <a href="<%="tradestock.jsp?stockId="+stockId%>"> <%=stockName%> </a></td></tr>
                <%
                        }
                    }
                %>
            </table>
        </div>
    </div>

        <jsp:include page="/footer.jspf"/>

