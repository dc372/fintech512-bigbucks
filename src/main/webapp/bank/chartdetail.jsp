<%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 4/6/22
  Time: 12:29 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.ibm.security.appscan.bigbucks.util.ChartUtil" %>
<%@ page import="org.jfree.chart.servlet.ServletUtilities" %>
<%@ page import="com.ibm.security.appscan.bigbucks.model.Index" %><%--
  Created by IntelliJ IDEA.
  User: dongchen
  Date: 4/3/22
  Time: 5:31 PM
  To change this template use File | Settings | File Templates.
--%>
<jsp:include page="/header.jspf"/>

<% boolean loggedIn = request.getSession().getAttribute("user") != null; %>

</style>

<div id="wrapper" style="width: 99%;">
    <jsp:include page="membertoc.jspf"/>
    <td valign="top" colspan="3" class="bb">
        <div class="fl" style="width: 99%;">
            <p>
                    <%
                        java.lang.String paramName = request.getParameter("stockId");
                        String error = "";
                        String filename = null;
                        try {
                            filename = ServletUtilities.saveChartAsPNG(ChartUtil.createReturnChart(paramName), 500, 270, null);

                        } catch (Exception e) {
                            error = e.getMessage();
                        }
                        String graphURL = request.getContextPath() + "/servlet/DisplayChart?filename="   + filename;
                %>
            <br>
            <img src="<%=graphURL%>" border="0" alt="<%=error%>" usemap="#<%=filename %>"/>
            </br>
            </p>

            <p>
                    <%
                        String error1 = "";
                        String filename1 = null;
                        try {
                            filename1 = ServletUtilities.saveChartAsPNG(ChartUtil.createReturn2DaysChart(paramName), 450, 450, null);
                        } catch (Exception e) {
                            error1 = e.getMessage();
                        }
                        String graphURL1 = request.getContextPath() + "/servlet/DisplayChart?filename="   + filename1;
                %>
            <br>
            <img src="<%=graphURL1%>" border="0" alt="<%=error1%>" usemap="#<%=filename1 %>"/>
            </br>

            </p>

            <p>
                    <%
                        String error2 = "";
                        String filename2 = null;
                        try {
                            filename2 = ServletUtilities.saveChartAsPNG(ChartUtil.createHistogramChart(paramName), 500, 270, null);
                        } catch (Exception e) {
                            error2 = e.getMessage();
                        }
                        String graphURL2 = request.getContextPath() + "/servlet/DisplayChart?filename="   + filename2;
                %>
            <br>
            <img src="<%=graphURL2%>" border="0" alt="<%=error2%>" usemap="#<%=filename2 %>"/>
            </br>

            </p>

            <p>
                    <%
                        String error3 = "";
                        String filename3 = null;
                        try {
                            filename3 = ServletUtilities.saveChartAsPNG(ChartUtil.createCumChart(paramName), 500, 270, null);
                        } catch (Exception e) {
                            error3 = e.getMessage();
                        }
                        String graphURL3 = request.getContextPath() + "/servlet/DisplayChart?filename="   + filename3;
                %>
            <br>
            <img src="<%=graphURL3%>" border="0" alt="<%=error3%>" usemap="#<%=filename3 %>"/>
            </br>

            </p>

            <p>
                    <%
                        String error4 = "";
                        String filename4 = null;
                        try {
                            filename4 = ServletUtilities.saveChartAsPNG(ChartUtil.createCompareChart(paramName), 600, 400, null);
                        } catch (Exception e) {
                            error4 = e.getMessage();
                        }
                        String graphURL4 = request.getContextPath() + "/servlet/DisplayChart?filename="   + filename4;
                %>
            <br>
            <img src="<%=graphURL4%>" border="0" alt="<%=error4%>" usemap="#<%=filename4 %>"/>
            </br>

            </p>

            <p>
                <%
                    String error5 = "";
                    String filename5 = null;
                    try {
                        filename5 = ServletUtilities.saveChartAsPNG(ChartUtil.createCAPMChart(paramName), 450, 450, null);
                    } catch (Exception e) {
                        error5 = e.getMessage();
                    }
                    String graphURL5 = request.getContextPath() + "/servlet/DisplayChart?filename="   + filename5;
                %>
                <br>
                <img src="<%=graphURL5%>" border="0" alt="<%=error5%>" usemap="#<%=filename5 %>"/>
                </br>

            </p>
        </div>
    </td>
</div>

<jsp:include page="/footer.jspf"/>
