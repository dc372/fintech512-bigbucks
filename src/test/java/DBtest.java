import com.ibm.security.appscan.bigbucks.model.TransactionSummary;
import com.ibm.security.appscan.bigbucks.util.APIUtil;
import com.ibm.security.appscan.bigbucks.util.DBUtil;
import org.junit.jupiter.api.Test;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.histquotes.Interval;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.ibm.security.appscan.bigbucks.util.APIUtil.getHistorical;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class DBtest {
    @Test
    public void test() throws SQLException {
        Calendar   cal   =   Calendar.getInstance();
        cal.add(Calendar.DATE,   -1);
        String yesterday = new SimpleDateFormat( "yyyy-MM-dd").format(cal.getTime());
        System.out.println(yesterday);

        String lastDate = DBUtil.getLastQuoteDatetime("MSFT");
        assertEquals(lastDate,yesterday);
        return;
    }

    @Test
    public void test1() throws ParseException {
        System.out.println(DBUtil._timestampToDateString(1648152004));
        System.out.println(DBUtil._timestampToDateString(1648166398));
        System.out.println(DBUtil._timestampToTimeString(1648166398));

        //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Timestamp timestamp = new Timestamp(1648152004*1000L);
        Date date = new Date(timestamp.getTime());

        // S is the millisecond
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy' 'HH:mm:ss:S");

        System.out.println(simpleDateFormat.format(timestamp));
        System.out.println(simpleDateFormat.format(date));
        System.out.println(simpleDateFormat.format(new Timestamp(1648166398*1000L)));
        System.out.println(APIUtil._getToday());
        System.out.println(APIUtil._countDays("2022-04-01","2022-04-02"));
    }

    @Test
    public void date_timestamp() {
        long time = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).parse("2022-03-24 16:00:04", new ParsePosition(0)).getTime() / 1000;
        Timestamp timestamp = new Timestamp(time*1000L);
        System.out.println(timestamp);
    }

    @Test
    public void searchTest() throws IOException, InterruptedException {
        System.out.println(APIUtil.getSearch("apple"));
    }

    @Test
    void stringTest(){

        System.out.println("^SPX".replaceAll("\\^","%5E"));

        String a = "^SPX".replaceAll("\\^","");
        System.out.println(a);
    }

    @Test
    void getAdjCloseTest() throws SQLException {
        System.out.println(DBUtil.getLastAdjCloseByDate("AAPL","2022-4-1"));
        System.out.println(DBUtil.getLastAdjCloseByDate("AAPL","2022-4-4"));
        System.out.println(DBUtil.getLastAdjCloseByDate("AAPL","2015-4-4"));
        System.out.println(DBUtil.getLastAdjClose("AAPL"));
    }

    @Test
    void getHistoricalDataTest() throws SQLException, IOException, ParseException, InterruptedException {
        System.out.println(getHistorical("GAPJ","price_adj_close"));
    }

    @Test
    void todayTimeTest() {
        DateFormat hourFormat = new SimpleDateFormat("HH");
        java.util.Date date = new java.util.Date();
        System.out.println(Integer.parseInt(hourFormat.format(date)));

        String date1 = APIUtil._getYesterday();
        long endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date1 + " 23:59:59", new ParsePosition(0)).getTime() / 1000;
        System.out.println(endTime);
    }

    @Test
    void yfinanceAPITest() throws IOException {
        Stock stock = YahooFinance.get("BABA");

        BigDecimal price = stock.getQuote().getPreviousClose();

        System.out.println(price);
        System.out.println(YahooFinance.get("BABA").getHistory());
        try {
            System.out.println(APIUtil.getHistorical("BABA","datetime"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    void risk_returnTest() {
        //String username, String datetime, long debitActId, String StockId, String tradeType, int share, double price
        //"yyyy-MM-dd HH:mm:ss"
        DBUtil.setTradeStocks("dchu","2022-02-03 15:37:32", 800200, "FB", "buy", 300, 235.9);
        DBUtil.setTradeStocks("dchu","2022-02-07 13:55:46", 800200, "FB", "buy", 100, 226.14);
        DBUtil.setTradeStocks("dchu","2022-02-08 14:31:32", 800200, "FB", "buy", 200, 217.83);
        DBUtil.setTradeStocks("dchu","2022-02-15 11:59:42", 800200, "FB", "buy", 200, 217.37);
        DBUtil.setTradeStocks("dchu","2022-02-03 14:52:47", 800200, "NFLX", "buy", 100, 406.35);
        DBUtil.setTradeStocks("dchu","2022-02-04 09:34:41", 800200, "PEP", "sell", 100, 174.5);
        DBUtil.setTradeStocks("dchu","2022-03-09 15:27:43", 800200, "PEP", "buy", 100, 158.13);
        DBUtil.setTradeStocks("dchu","2022-02-07 15:00:23", 800200, "TSLA", "buy", 100, 919);
        DBUtil.setTradeStocks("dchu","2022-03-09 15:28:14", 800200, "TSLA", "buy", 200, 858.54);
        DBUtil.setTradeStocks("dchu","2022-02-01 14:05:42", 800200, "TSM", "buy", 100, 122.03);
        DBUtil.setTradeStocks("dchu","2022-02-03 14:55:01", 800200, "TSM", "buy", 300, 120.3);
        DBUtil.setTradeStocks("dchu","2022-04-06 13:55:46", 800200, "TSM", "buy", 100, 100.65);
    }

    @Test
    void adminTest() throws SQLException {
        ArrayList<TransactionSummary> tss = DBUtil.get1dayAllTransactions(APIUtil._getYesterday());
        for (TransactionSummary ts: tss) {
            System.out.println(ts.getStockId()+"::"+ts.getBuyShare()+"::"+ts.getSellShare());
        }
    }

    @Test
    void addStockTest() {
        String StockId = "AWH";
        String StockName = "Aspira Women's Health Inc.";
        System.out.println(StockName);
        System.out.println(DBUtil.addStock(StockId, StockName));
    }

    @Test
    void ApiTest() throws IOException {
        String symbol = "GAPJ";

        Calendar from = Calendar.getInstance();
        Calendar to = Calendar.getInstance();
        from.add(Calendar.YEAR, Integer.parseInt("-1"));

        yahoofinance.Stock stock = YahooFinance.get(symbol);

        List<HistoricalQuote> history = stock.getHistory(from, Interval.DAILY);


        System.out.println(history);
    }

    @Test
    void parseIntTest() {
        if ("2022-04-11".equals(APIUtil._getToday()) && (((APIUtil._getHour() < 16) || (APIUtil._getHour() == 16 && APIUtil._getMinute() < 10))) ){
            System.out.println("111");
        }
    }
}
